package editor.controller;

import static common.model.Texture.TEXTURE_FRUIT_SPAWN;
import static common.model.Texture.TEXTURE_GHOST_SPAWN;
import static common.model.Texture.TEXTURE_PACMAN_SPAWN;
import static common.model.Texture.TEXTURE_SUPERGUM_SPAWN;
import static common.model.Texture.TEXTURE_WALL;

import editor.view.EditorRootPane;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;

/**
 * Handles the different key events in the editor.
 */
public class KeyListenerEditor implements EventHandler<KeyEvent>{

	private EditorRootPane _me;
	
	// KeyCodeConbination recognizing CTRL + S
	private final KeyCodeCombination comb = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
	
	public KeyListenerEditor(){
		_me = EditorRootPane.getInstance();
	}
	
	
	@Override
	public void handle(KeyEvent event) {
		// TODO Auto-generated method stub
		if(comb.match(event)){
			_me.requestFocus();
			_me.export();
		}
		else{
			switch(event.getCode()){
			case A:
				_me.setCurrentColor(TEXTURE_WALL);
				break;
			case E:
				_me.setCurrentColor(TEXTURE_GHOST_SPAWN);
				break;
			case Z:
				_me.setCurrentColor(TEXTURE_PACMAN_SPAWN);
				break;
			case R:
				if(event.isControlDown())
					System.out.println("CTRL");
				_me.setCurrentColor(TEXTURE_FRUIT_SPAWN);
				break;
			case T:
				_me.setCurrentColor(TEXTURE_SUPERGUM_SPAWN);
				break;
				
			case ESCAPE:
				_me.openCloseMenu();
				break;
			}
		}
		event.consume();
	}

}
