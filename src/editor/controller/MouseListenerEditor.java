package editor.controller;

import editor.view.EditorRootPane;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 * Handles the mouse events in the editor.
 */
public class MouseListenerEditor implements EventHandler<MouseEvent>{

	EditorRootPane _me;
	
	public MouseListenerEditor(){
		_me = EditorRootPane.getInstance();
	}
	
	@Override
	public void handle(MouseEvent event) {
		if(! _me.isOpened()){
		
			_me.requestFocus();
			
			boolean rightButton = ( event.getButton() == MouseButton.SECONDARY ); 
			_me.changeColor((int)event.getSceneX(), (int)event.getSceneY(), rightButton);
		}
	}

}
