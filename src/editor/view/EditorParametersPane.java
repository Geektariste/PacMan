package editor.view;

import common.model.Texture;
import common.view.NumberTextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import xml.model.Map;

public class EditorParametersPane extends GridPane{
	
	private Map map;
	
	private ImageView image_currentTexture;
	
	private Text text_currentTexture;
	private Text text_size;
	private Text text_timerFruit;
	private Text text_timerSuperg;
	private Text text_ghosts;
	
	private NumberTextField field_size;
	private NumberTextField field_timerFruit;
	private NumberTextField field_timerSuperg;
	private NumberTextField field_ghosts;
	
	public EditorParametersPane(){
		super();
		
		map = EditorRootPane.getInstance().getLaby().getMap();
		
		/*
		 * Legend
		 */
		
		text_currentTexture = initText("Current");
		text_size = initText("Size");
		text_timerFruit = initText("Fruit timer");
		text_timerSuperg = initText("SuperGum timer");
		text_ghosts = initText("Ghosts");
		
		/*
		 * Controls
		 */
		
		image_currentTexture = new ImageView(Texture.getCurrentTexture().getTexture());
		
		field_size = new NumberTextField();
		field_timerFruit = new NumberTextField();
		field_timerSuperg = new NumberTextField();
		field_ghosts = new NumberTextField();
		
		/*
		 * Feed with content
		 */
		
		field_size.setValue(map.getGridSize());
		field_timerFruit.setValue(map.getFruitTimer());
		field_timerSuperg.setValue(map.getSuperTimer());
		field_ghosts.setValue(map.getNbGhosts());
		
		/*
		 * Bindings on focus loss
		 */
		
		field_timerFruit.focusedProperty().addListener(new ChangeListener<Boolean>(){

			@Override
			public void changed(ObservableValue<? extends Boolean> property, Boolean oldValue, Boolean newValue) {
				if(! newValue)
					map.setFruitTimer(field_timerFruit.getValue());
			}
			
		});
		
		field_timerSuperg.focusedProperty().addListener(new ChangeListener<Boolean>(){

			@Override
			public void changed(ObservableValue<? extends Boolean> property, Boolean oldValue, Boolean newValue) {
				if(! newValue)
					map.setSuperTimer(field_timerSuperg.getValue());
			}
			
		});
		
		field_ghosts.focusedProperty().addListener(new ChangeListener<Boolean>(){

			@Override
			public void changed(ObservableValue<? extends Boolean> property, Boolean oldValue, Boolean newValue) {
				if(! newValue)
					map.setNbGhosts(field_ghosts.getValue());
			}
			
		});
		
		field_size.focusedProperty().addListener(new ChangeListener<Boolean>(){
			
			@Override
			public void changed(ObservableValue<? extends Boolean> property, Boolean oldValue, Boolean newValue) {	
				if (! newValue)
					EditorRootPane.getInstance().changeMapSize(field_size.getValue());
			}
		});
		
		this.add(image_currentTexture, 1, 1);
		this.add(text_currentTexture, 2, 1);

		this.add(text_size, 1, 2);
		this.add(text_timerFruit, 1, 3);
		this.add(text_timerSuperg, 1, 4);
		this.add(text_ghosts, 1, 5);
		
		this.add(field_size, 2, 2);
		this.add(field_timerFruit, 2, 3);
		this.add(field_timerSuperg, 2, 4);
		this.add(field_ghosts, 2, 5);
		
	}
	
	public static Text initText(String text){
		Text res = new Text(text);
		res.setFill(Color.BLACK);
		res.setFont(new Font(20));
		
		return res;
	}
	
	public void setCurrentTexture(Texture color){
		Texture.setCurrentTexture(color);
		updateCurrentTexture();
	}
	
	public void updateCurrentTexture(){
		image_currentTexture.setImage(Texture.getCurrentTexture().getTexture());
	}
	
}
