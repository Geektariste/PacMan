package editor.view;

import java.io.File;

import common.model.DefaultString;
import common.model.Texture;
import common.view.AppWithMenuPane;
import common.view.EditableText;
import editor.controller.KeyListenerEditor;
import editor.controller.MouseListenerEditor;
import game.model.Labyrinth;
import javafx.geometry.Point2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import xml.XmlSerialization;
import xml.model.ScoreBoard;

public class EditorRootPane extends AppWithMenuPane {
	
	private static EditorRootPane INSTANCE = null;

	private BorderPane _root;

	private EditableText _nameView;
	private EditorLegendPane _legendView;
	private EditorParametersPane _parametersView;
	private EditorLabyViewPane _labyView;
	
	private Labyrinth _laby;

	public EditorRootPane(Labyrinth laby) {
		
		super();
		
		INSTANCE = this;

		_laby = laby;
		
		initMainPanes();
		initListeners();
	}

	public void initMainPanes() {
		
		_menu = new EditorMenuPane(this);
		
		_nameView = new EditableText("Name", _laby.getMap());
		_labyView = new EditorLabyViewPane();
		_legendView = new EditorLegendPane();
		_parametersView = new EditorParametersPane();

		_nameView.setId("mapName");
		
		_root = new BorderPane();

		_root.setTop(_nameView);
		_root.setCenter(_labyView);
		_root.setLeft(_legendView);
		_root.setRight(_parametersView);
		
		this.getChildren().add(_root);
	}

	public void initListeners() {
		this.setOnKeyPressed(new KeyListenerEditor());
		MouseListenerEditor mle = new MouseListenerEditor();
		this.setOnMousePressed(mle);
		this.setOnMouseDragged(mle);
	}

	/**
	 * Change the color of the Canvas detected at this position.
	 */
	public void changeColor(int x, int y, boolean rightButton) {
		Point2D localCoords = _labyView.sceneToLocal(x, y);
		_labyView.changeTexture((int)localCoords.getX(), (int)localCoords.getY(), rightButton);
	}

	public void changeMapSize(int value) {
		_labyView.changeMapSize(value);
	}
	
	public void export(){

		if (_laby.getMap().getName().isEmpty()){
			
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("Untitled map");
			alert.setContentText("You must name your map to save it !");
			alert.showAndWait();
		}

		else {

			FileChooser fileChooser = new FileChooser();
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
					DefaultString.FILE_CHOOSER_DESCRIPTION, DefaultString.FILE_CHOOSER_ALL_MAPS);
			
			fileChooser.getExtensionFilters().add(extFilter);
			fileChooser.setTitle("Save As");
			fileChooser.setInitialDirectory(new File(DefaultString.MAPS));
			fileChooser.setInitialFileName(_laby.getMap().getFileName());

			File file = fileChooser.showSaveDialog(null);

			if (file != null) {
				String scoreName = _laby.getMap().getName();
				File scoreFile = new File(DefaultString.SCOREBOARDS + scoreName + DefaultString.SCORE_EXTENSION);
				XmlSerialization.getInstance().serialize(_laby.getMap(), file);
				XmlSerialization.getInstance().serialize(new ScoreBoard(scoreName), scoreFile);
			}
		}
	}

	/*
	 * Modifies the current Color.
	 */
	public void setCurrentColor(Texture texture) {
		_parametersView.setCurrentTexture(texture);
	}

	@Override
	public void openCloseMenu() {

		if (!_opened_menu)
			this.getChildren().add(_menu);
		else
			this.getChildren().remove(_menu);

		_opened_menu = !_opened_menu;
	}
	
	public void refreshLabyView() {
		_labyView = new EditorLabyViewPane();
		_root.setCenter(_labyView);
	}

	public Labyrinth getLaby(){
		return _laby;
	}
	
	public static EditorRootPane getInstance(){
		return INSTANCE;
	}

	
}
