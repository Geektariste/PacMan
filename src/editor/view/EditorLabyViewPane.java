package editor.view;

import static common.model.Texture.*;

import common.model.Texture;
import game.model.Case;
import game.model.Labyrinth;
import game.model.SuperPacGum;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import xml.model.Map;

public class EditorLabyViewPane extends GridPane {
		
	private Labyrinth _laby;
	
	public EditorLabyViewPane() {
		
		_laby = EditorRootPane.getInstance().getLaby();
		
		refreshDisplay();
	}
	
	public void refreshDisplay(){
		
		this.getChildren().clear();
		
		int width = _laby.getGridSize();
		Map grid = _laby.getMap();
		
		for (int line = 0; line < width; line++) {
			for (int column = 0; column < width; column++) {
				
				Node newNode;
				
				if(! grid.getCase(column, line).isCrossable())
					newNode = new ImageView(TEXTURE_WALL.getTexture());
				
				else if(grid.getCase(column, line) == _laby.getMap().getPlayerSpawn())
					newNode = Texture.createSpawnImage(TEXTURE_PACMAN_SPAWN);

				else if(grid.getCase(column, line) == _laby.getMap().getGhostsSpawn())
					newNode = Texture.createSpawnImage(TEXTURE_GHOST_SPAWN);
				
				else if(grid.getCase(column, line) == _laby.getMap().getFruitSpawn())
					newNode = Texture.createSpawnImage(TEXTURE_FRUIT_SPAWN);
				
				else if(grid.getCase(column, line).getObject() instanceof SuperPacGum)
					newNode = Texture.createSpawnImage(TEXTURE_SUPERGUM_SPAWN);
				
				else
					newNode = new ImageView(TEXTURE_GROUND.getTexture());

				
				this.add(newNode, column, line);
			}
		}		
	}

	public void changeTexture(int x, int y, boolean rightButton) {
		
		int column = x / Case.SIZE;
		int line = y / Case.SIZE;
		
		int width = _laby.getGridSize();
		
		if (column >= 0 && column < width && line >= 0 && line < width) {
			
			Case to_change = EditorRootPane.getInstance().getLaby().getMap().getCase(column, line);
			
			Node newNode;
			
			Node oldSpawnNode;
			Case oldSpawnCase;

			if(to_change == _laby.getMap().getGhostsSpawn())
				_laby.getMap().setGhostsSpawn(null);
			if(to_change == _laby.getMap().getPlayerSpawn())
				_laby.getMap().setPlayerSpawn(null);
			if(to_change == _laby.getMap().getFruitSpawn())
				_laby.getMap().setFruitSpawn(null);
			if(to_change.getObject() instanceof SuperPacGum){
				to_change.setObject(null);
			}
						
			if (rightButton) {
				newNode = new ImageView(TEXTURE_GROUND.getTexture());
				to_change.setCrossable(true);
			}
			else if (Texture.getCurrentTexture() == TEXTURE_WALL) {
				newNode = new ImageView(TEXTURE_WALL.getTexture());
				to_change.setCrossable(false);
			} 			
			else if (Texture.getCurrentTexture() == TEXTURE_SUPERGUM_SPAWN){
				newNode = Texture.createSpawnImage();
				to_change.setObject(new SuperPacGum(to_change.getPosX()/Case.SIZE, to_change.getPosY()/Case.SIZE, 10));
				to_change.setCrossable(true);
			}
			
			else{
				
				if (Texture.getCurrentTexture() == TEXTURE_GHOST_SPAWN){
					oldSpawnCase = _laby.getMap().getGhostsSpawn();
					_laby.getMap().setGhostsSpawn(_laby.getMap().getCase(column, line));
				}
				else if (Texture.getCurrentTexture() == TEXTURE_PACMAN_SPAWN){
					oldSpawnCase = _laby.getMap().getPlayerSpawn();
					_laby.getMap().setPlayerSpawn(_laby.getMap().getCase(column, line));
				}
				
				else{
					oldSpawnCase = _laby.getMap().getFruitSpawn();
					_laby.getMap().setFruitSpawn(_laby.getMap().getCase(column, line));
				}
				
				if(oldSpawnCase != null && oldSpawnCase != to_change){
					
					oldSpawnNode = this.getNode(oldSpawnCase.getPosX() / Case.SIZE, oldSpawnCase.getPosY() / Case.SIZE);
					this.getChildren().remove(oldSpawnNode);
					this.add(new ImageView(TEXTURE_GROUND.getTexture()), oldSpawnCase.getPosX() / Case.SIZE, oldSpawnCase.getPosY() / Case.SIZE);
					
					newNode = Texture.createSpawnImage();
					
				}
				
				else
					newNode = Texture.createSpawnImage();
				
				to_change.setCrossable(true);
			}

			this.add(newNode, column, line);
		}
			
	}
	
	public void changeMapSize(int newSize){
		
		int oldSize = EditorRootPane.getInstance().getLaby().getGridSize();

		if (oldSize > newSize) {
			// Prompt : T'es sur ? Perte d'information
		}
		
		Map map = _laby.getMap();

		if (oldSize < newSize) {
			
			// Add on old lines
			
			for(int line = 0; line < oldSize; line++){
				for(int column = oldSize; column < newSize; column++)
					map.addCase(column, line);
			}
			
			// Add on new lines
			
			for(int line = oldSize; line < newSize; line++){
				map.addLine();
				for(int column = 0; column < newSize; column++)
					map.addCase(column, line);
			}
		}
		
		else{
			
			// Delete old columns
			
			for(int line = newSize - 1; line >= 0 ; line--){
				for(int column = oldSize - 1; column >= newSize; column--)
					map.removeCase(column, line);
			}
			
			// Delete old lines
			
			for(int line = oldSize - 1; line >= newSize; line--){
				map.removeLine(line);
			}

		}

		map.setGridSize(newSize);
		refreshDisplay();
	}
	
	public Node getNode(int column, int line) {
		Node result = null;
		ObservableList<Node> childrens = this.getChildren();
		for (Node node : childrens) {
			if (GridPane.getRowIndex(node) == line && GridPane.getColumnIndex(node) == column) {
				result = node;
				break;
			}
		}
		return result;
	}

}
