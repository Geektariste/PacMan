package editor.view;

import common.MainWindow;
import common.view.MainMenuPane;
import common.view.MenuPane;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

public class EditorMenuPane extends MenuPane {
	
	private EditorRootPane _editor;
	
	public EditorMenuPane(EditorRootPane editor){
		
		super();
		
		this.getStyleClass().add("game-menu-pane");
		
		_editor = editor;
		
		this.getStyleClass().add("editor-menu-pane");
		
		Label tMenu = new Label("Menu");
		tMenu.setFont(new Font(80));
		
		Label tLoad = new Label("Load");
		tLoad.setFont(new Font(30));
		
		Label tSave = new Label("Save");
		tSave.setFont(new Font(30));
		
		Label tGoToMenu = new Label("Return to main menu");
		tGoToMenu.setFont(new Font(30));
		
		tLoad.setOnMouseClicked(new EventHandler(){
			@Override
			public void handle(Event event) {
				; 
			}
		});
		
		tSave.setOnMouseClicked(new EventHandler(){
			@Override
			public void handle(Event event) {
				_editor.export();
			}
		});
		
		tGoToMenu.setOnMouseClicked(new EventHandler(){
			@Override
			public void handle(Event event) {
				MainWindow.getInstance().switchPane(new MainMenuPane());
			}
		});
		
		_menu.getChildren().addAll(tLoad, tSave, tGoToMenu);
		
		
		
		this.setTop(tMenu);
		this.setCenter(_menu);
	}

}
