package editor.view;

import game.model.Labyrinth;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import xml.XmlSerialization;
import xml.model.LoadMapBuffer;
import xml.model.Map;

import common.MainWindow;
import common.exception.InvalidFormatException;
import common.exception.InvalidStructureException;
import common.model.DefaultString;
import common.view.MainMenuPane;

public class EditorMapSelectionPane extends BorderPane{
	
	private ScrollPane _scroll;
	private VBox _content;
	
	public EditorMapSelectionPane(){
		
		this.getStyleClass().add("map-selection-pane");
		
		_scroll = new ScrollPane();
		_content = new VBox();
		
		_scroll.setContent(_content);
		
		Text title = new Text("Select map");
		title.setId("title");
		
		this.setTop(title);
		this.setCenter(_scroll);
		
		loadMaps();	
		
		VBox vbox = new VBox();
		

		
		Button newMapButton = new Button("New Map");
		newMapButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		    	loadDefaultMap();
		    }
		});
		vbox.getChildren().add(newMapButton);
		
		Button returnButton = new Button("Back to main menu");
		returnButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        MainWindow.getInstance().switchPane(new MainMenuPane());
		    }
		});
		vbox.getChildren().add(returnButton);
		
		this.setBottom(vbox);
	}
	
	private void loadMaps(){
		File[] maps = new File(DefaultString.MAPS).listFiles();

		for (File map : maps)
			if (map.getName().endsWith(DefaultString.MAP_EXTENSION)) {
				try {
					LoadMapBuffer.clear();
					Map loaded_map = (Map) XmlSerialization.getInstance().deserialize(
							new FileInputStream(map),	
							map.getName());
					
					_content.getChildren().add(	new MapDisplayPane(loaded_map));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
	}
	
	private void loadDefaultMap(){
		File map = new File(DefaultString.DEFAULT_MAP);
		try {
			
			LoadMapBuffer.clear();
			Map loaded_map = (Map) XmlSerialization.getInstance().deserialize(new FileInputStream(map), map.getName());
			Labyrinth _loadedMap = new Labyrinth(loaded_map.getName(), loaded_map);
			MainWindow.getInstance().switchPane(new EditorRootPane(_loadedMap));
			
		} catch (FileNotFoundException | InvalidFormatException | InvalidStructureException e) {
			e.printStackTrace();
		}
	}
	
	private class MapDisplayPane extends BorderPane{
		
		private Labyrinth _loadedMap;
		private Map _map;
		
		public MapDisplayPane(Map map) { 
			_map = map;
			try {
				_loadedMap = new Labyrinth(_map.getName(), _map);
				this.setRight(new Text("IMAGE"));
				
				this.setOnMouseClicked(new EventHandler<MouseEvent>(){
					@Override
					public void handle(MouseEvent arg0) {
							MainWindow.getInstance().switchPane(new EditorRootPane(_loadedMap));
					}
				});
			} 
			
			catch (InvalidFormatException e) {
				this.setRight(new Text("Corrupted Map"));
			}
			
			/*
			 * A modifier, pas très propre
			 */
			
			catch (InvalidStructureException e){
				this.setRight(new Text("IMAGE MEME SI PAS BONNE"));
				
				try {
					_loadedMap = new Labyrinth(_map.getName(), _map, true);
				} catch (InvalidFormatException | InvalidStructureException e1) {
					e1.printStackTrace();
				}
				
				
				this.setOnMouseClicked(new EventHandler<MouseEvent>(){
					@Override
					public void handle(MouseEvent arg0) {
						MainWindow.getInstance().switchPane(new EditorRootPane(_loadedMap));
					}
				});
			}
			
			this.setLeft(new Text(_map.getName()));
			
		}
		
	}

}
