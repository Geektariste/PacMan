package editor.view;

import static common.model.Texture.*;

import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import common.model.Texture;

public class EditorLegendPane extends GridPane{

	private ImageView texture_wall;
	private ImageView texture_pacgum;
	
	private Group texture_pacman;
	private Group texture_ghost;
	private Group texture_fruit;	
	private Group texture_supergum;
	
	private Text text_pacgum;
	private Text text_supergum;
	private Text text_wall;
	private Text text_pacman;
	private Text text_ghost;
	private Text text_fruit;
	private Text text_save;
	
	private int _width;
	
	public EditorLegendPane(){
		
		super();
		
		texture_pacgum = new ImageView(TEXTURE_GROUND.getTexture());
		texture_wall = new ImageView(TEXTURE_WALL.getTexture());
		
		texture_pacman = Texture.createSpawnImage(TEXTURE_PACMAN_SPAWN);
		texture_fruit = Texture.createSpawnImage(TEXTURE_FRUIT_SPAWN);
		texture_ghost = Texture.createSpawnImage(TEXTURE_GHOST_SPAWN);
		texture_supergum = Texture.createSpawnImage(TEXTURE_SUPERGUM_SPAWN);

		
		text_pacgum = initText("Ground (Right click)");
		text_supergum = initText("SuperPacGum (T key)");
		text_wall = initText("Wall (A key)");
		text_pacman = initText("PacMan (Z key)");
		text_ghost = initText("Ghosts (E key)");
		text_fruit = initText("Fruit (R key)");
		text_save = initText("Save : CTRL + S");
		
		this.add(texture_pacgum, 1, 1);
		this.add(texture_wall, 1, 2);
		this.add(texture_pacman, 1, 3);
		this.add(texture_ghost, 1, 4);
		this.add(texture_fruit, 1, 5);
		this.add(texture_supergum, 1, 6);
		
		this.add(text_pacgum, 2, 1);
		this.add(text_wall, 2, 2);
		this.add(text_pacman, 2, 3);
		this.add(text_ghost, 2, 4);
		this.add(text_fruit, 2, 5);
		this.add(text_supergum, 2, 6);
		
		this.add(text_save, 2, 7);	
	}
	
	
	public static Text initText(String text){
		Text res = new Text(text);
		res.setFill(Color.BLACK);
		res.setFont(new Font(20));
		
		return res;
	}

}
