package leaderboard.view;

import java.io.File;

import common.MainWindow;
import common.model.DefaultString;
import game.view.GameRootPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import xml.XmlSerialization;
import xml.model.RegisteredScore;
import xml.model.ScoreBoard;

public class HiScorePane extends Pane {
	
	private Text _text;
	private TextField _textField;
	private Button _validate;
	private ScoreBoard _sb;
	
	public HiScorePane(ScoreBoard sb){
		
		_sb = sb;
		_text = new Text("Enter ya name ->");
		_textField = new TextField("Bobby");
		_validate = new Button("Submit");
		
		HBox hbox = new HBox();
		hbox.getChildren().addAll(_text, _textField, _validate);
		this.getChildren().add(hbox);
		_validate.setOnAction(new EventHandler<ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(!_textField.getText().isEmpty()){
					_sb.addScore(new RegisteredScore(GameRootPane.getInstance().getLaby().getScore(), _textField.getText()));
					File board = new File(DefaultString.SCOREBOARDS+_sb.getFileName());
					XmlSerialization.getInstance().serialize(_sb, board);
					MainWindow.getInstance().switchPane(new LeaderboardRootPane(board));
				}
			}
			
		});
	}

}
