package leaderboard.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import common.MainWindow;
import common.view.MainMenuPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import xml.XmlSerialization;
import xml.model.RegisteredScore;
import xml.model.ScoreBoard;

public class LeaderboardRootPane extends BorderPane {
	
	private File _file;
	private ScrollPane _scroll;
	private VBox _content;
	private ScoreBoard _scoreBoard;
	public LeaderboardRootPane(File file){
		
		_file = file;
		_scroll = new ScrollPane();
		_content = new VBox();
		_scroll.setContent(_content);
		this.setCenter(_scroll);
		try {
			_scoreBoard = (ScoreBoard) XmlSerialization.getInstance().deserialize(new FileInputStream(_file), _file.getName());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		//System.out.println(_scoreBoard);
		print();
		
		Button returnButton = new Button("Back to map selection");
		returnButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        MainWindow.getInstance().switchPane(new LeaderboardMapSelectionPane());
		    }
		});
		
		Button returnToMainButton = new Button("Back to main menu");
		returnToMainButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        MainWindow.getInstance().switchPane(new MainMenuPane());
		    }
		});
		
		VBox buttons = new VBox();
		buttons.getChildren().addAll(returnButton, returnToMainButton);
		this.setBottom(buttons);
	}
		

	public void print(){
		int i = 1;
		for (RegisteredScore rs : _scoreBoard.getScoreList()){
			Text score = new Text(i+") "+rs.toString());
			_content.getChildren().add(score);
			i++;
		}
	}
}
