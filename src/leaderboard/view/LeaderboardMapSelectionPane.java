package leaderboard.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import common.MainWindow;
import common.exception.InvalidFormatException;
import common.exception.InvalidStructureException;
import common.model.DefaultString;
import common.view.MainMenuPane;
import game.model.Labyrinth;
import game.view.GameRootPane;
//import game.view.MapSelectionPane.MapDisplayPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import xml.XmlSerialization;
import xml.model.LoadMapBuffer;
import xml.model.Map;
import xml.model.ScoreBoard;

public class LeaderboardMapSelectionPane extends BorderPane{
	
	private ScrollPane _scroll;
	private VBox _content;
	
	
	public LeaderboardMapSelectionPane(){
		_scroll = new ScrollPane();
		_content = new VBox();
		
		_scroll.setContent(_content);
		
		this.setTop(new Text("Select map"));
		this.setCenter(_scroll);
		
		loadBoards();	
		
		Button returnButton = new Button("Back to main menu");
		returnButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        MainWindow.getInstance().switchPane(new MainMenuPane());
		    }
		});
		
		this.setBottom(returnButton);
		

	}
	private void loadBoards(){
		File[] boards = new File(DefaultString.SCOREBOARDS).listFiles();

		for (File board : boards){
		

			
			if (board.getName().endsWith(DefaultString.SCORE_EXTENSION)) {
				String str = board.getName();
				int pos = str.lastIndexOf(".");
				Text name = new Text(str.substring(0, pos));
				name.setOnMouseClicked(new EventHandler<MouseEvent>(){
				@Override
				public void handle(MouseEvent arg0) {
					//System.out.println("load : " + board.getName());
					MainWindow.getInstance().switchPane(new LeaderboardRootPane(board));
				}
});
				//System.out.println("load : " + board.getName());
				_content.getChildren().add( name );
			}
		}
		
	}
	
	
}
