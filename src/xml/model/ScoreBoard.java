package xml.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import common.model.DefaultString;
import xml.XmlSerialization;

public class ScoreBoard extends XmlSerializableObject{

	private ArrayList<RegisteredScore> scoreList;
	private String _name;
	private static transient int listSize = 10;
	
	public ScoreBoard(String name){
		scoreList = new ArrayList<RegisteredScore>();
		_name = name;
	}
	
	public ArrayList<RegisteredScore> getScoreList(){ return scoreList; }
	public String getName(){ return _name; }
	
	
	public void setScoreList(ArrayList<RegisteredScore> list) { scoreList = list; }
	public void setName(String name){ _name = name; }
	
	public void eraseList(){
		for(int i = 0; i<scoreList.size(); i++){
			scoreList.remove(i);
		}
	}
		
	public int getMinimum(){
		if(getScoreList().isEmpty()){
			return 0;
		}
		else return getScoreList().get((getScoreList().size()-1)).getScore();
	}
	
	public boolean isFull(){
		return !(getScoreList().size() < listSize);
	}
	
	public void addScore(RegisteredScore rs){
		if(isFull()){
			if(rs.getScore() > scoreList.get(listSize-1).getScore()){
			scoreList.remove(listSize-1);
			scoreList.add(rs);
			}
		}
		else{
			scoreList.add(rs);
		}
		this.sortByScore();
	}
	
	class ScoreComparator implements Comparator<RegisteredScore>{
		
		public int compare(RegisteredScore rs1, RegisteredScore rs2){
			if(rs1.getScore() < rs2.getScore())
				return 1;
			else 
				return -1;
		}
	}
	
	public void sortByScore(){
		Collections.sort(scoreList, new ScoreComparator());
	}
	
	public String toString(){
		String res = "";
		for (int i = 0; i < scoreList.size(); i++){
			res+=scoreList.get(i);
		}
		return res;
	}
	
}
