package xml.model;

import java.util.ArrayList;

import game.model.Case;

/**
 * Represents a single line in a Map.
 */
public class Line{
	
	private ArrayList<Case> _content;
	private transient Map _parent;
	
	public Line(Map parent){
		super();
		_content = new ArrayList<Case>();
		_parent = parent;
	}
	
	/*
	 * Getters
	 */
	
	public Map getParent(){ return _parent; }
	public Case get(int index){ return _content.get(index); }
	
	/*
	 * Setters
	 */
	
	public void setParent(Map parent){ _parent = parent; }
	public void add(Case cell){ _content.add(cell); }
	public void remove(int index){ _content.remove(index); }
	
	public String toString(){
		String str = "| ";
		for(Case cell : _content)
			str+= cell.toString()+" | ";
		
		return str;
	}
}