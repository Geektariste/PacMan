package xml.model;

import game.model.Case;
import game.model.Fruit;
import game.model.Labyrinth;
import game.model.SuperPacGum;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Is used to convert a Case in order to save it with the serialization.
 */
public class CaseConverter implements Converter {
	
	private static final String S_WALL = "Wall";
	private static final String S_SUPER = "Super PacGum";
	private static final String S_GROUND = "Ground";
	private static final String S_PACMAN = "PacMan";
	private static final String S_GHOST = "Ghosts";
	private static final String S_FRUIT = "Fruit";
	
	public boolean canConvert(Class c){
		return c.equals(Case.class);
	}

	@Override
	public void marshal(Object o, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		// TODO Auto-generated method stub
		
		Case c = (Case) o;
		try{
		if(!c.isCrossable())
			writer.setValue(S_WALL);
		// Statics
		else if(c.getObject() instanceof SuperPacGum)
			writer.setValue(S_SUPER);
		else if(c.equals(c.getParent().getParent().getFruitSpawn()))
			writer.setValue(S_FRUIT);
		// Dunamics
		else if(c.equals(c.getParent().getParent().getPlayerSpawn()))
			writer.setValue(S_PACMAN);
		else if(c.equals(c.getParent().getParent().getGhostsSpawn()))
			writer.setValue(S_GHOST);
		else
			writer.setValue(S_GROUND);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

		
			

	@Override
	public Object unmarshal(HierarchicalStreamReader reader,
			UnmarshallingContext context) {
		
		String value = reader.getValue();
		Case res = new Case(0, 0, true, null);
		
		if(value.equals(S_WALL))
			res.setCrossable(false);
		else if(value.equals(S_GHOST))
			LoadMapBuffer.ghostSpawn = res;
		else if(value.equals(S_PACMAN))
			LoadMapBuffer.pacmanSpawn = res;
		else if(value.equals(S_FRUIT))
			LoadMapBuffer.fruitspawn = res;
		else if(value.equals(S_SUPER))
			LoadMapBuffer.superPos.add(res);

		return res;
	}

}
