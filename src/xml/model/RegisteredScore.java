package xml.model;

import java.util.Comparator;

public class RegisteredScore {
	
	private int _score;
	private String _name;
	
	public RegisteredScore(int score, String name){
		_score = score;
		_name = name;
	}
	
	public int getScore(){ return _score; }
	public String getName(){ return _name; }
	
	public void setScore(int score){ _score = score; }
	public void setName(String name){ _name = name; }
	
	public String toString(){
		return "" + _name + " " + _score + "pts"; 
	}


	
}
