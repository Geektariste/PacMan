package xml.model;

import java.util.ArrayList;

import game.model.Case;

/**
 * This class is used when we load a Map with deserialize.
 */
public class LoadMapBuffer {
	public static Case ghostSpawn;
	public static Case pacmanSpawn;
	public static Case fruitspawn;
	public static ArrayList<Case> superPos = new ArrayList<Case>();
	
	/**
	 * Cleans the buffer for the next use.
	 */
	public static void clear(){
		ghostSpawn = null;
		pacmanSpawn = null;
		fruitspawn = null;
		superPos.clear();
	}
}
