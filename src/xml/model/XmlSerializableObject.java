package xml.model;

public class XmlSerializableObject {

	private transient String _filePath;
	private transient String _fileName;
	
	public String getFilePath(){ return _filePath; }
	public String getFileName(){ return _fileName; }
	
	public void setFileName(String fileName) { _fileName = fileName; }
	public void setFilePath(String filePath) { _filePath = filePath; }
	

	
}
