package xml.model;

import java.util.ArrayList;

import game.model.Case;
import game.model.Fruit;
import game.model.Labyrinth;
import game.model.PacGum;
import game.model.SuperPacGum;

/**
 * Contains the details of the different Cases.
 */
public class Map extends XmlSerializableObject{

	private String _name;
	private int _gridSize;
	private int _nbGhosts;
	private int _fruitTimer;
	private int _superTimer;
	private ArrayList<Line> _grid;
	
	private transient Case _fruitSpawn;
	private transient Case _playerSpawn;
	private transient Case _ghostsSpawn;
	private transient Labyrinth _parent;
	
	private transient ScoreBoard _scoreBoard;
	
	/*
	 * Getters
	 */

	public String getName() { return _name; }
	public int getGridSize() { return _gridSize; }
	public int getNbGhosts() { return _nbGhosts; }
	public int getFruitTimer() { return _fruitTimer; }
	public int getSuperTimer() { return _superTimer; }
	public ArrayList<Line> getGrid() { return _grid; }
	public Case getFruitSpawn(){ return _fruitSpawn; }
	public Case getPlayerSpawn(){ return _playerSpawn; }
	public Case getGhostsSpawn(){ return _ghostsSpawn; }
	public ScoreBoard getScoreBoard(){ return _scoreBoard; }

	public Case getCase(int column, int line){ 
		return _grid.get(line).get(column); 
	}
	
	/*
	 * Setters
	 */
	
	public void setName(String name){ _name = name; }
	public void setGridSize(int gridSize){ _gridSize = gridSize; }
	public void setNbGhosts(int nb_gh){ _nbGhosts = nb_gh; }
	public void setFruitTimer(int fruitTimer){ _fruitTimer = fruitTimer; }
	public void setSuperTimer(int superTimer){ _superTimer = superTimer; }
	public void setGrid(ArrayList<Line> grid){ _grid = grid; }
	public void setFruitSpawn(Case c){ _fruitSpawn = c; }
	public void setPlayerSpawn(Case c){	_playerSpawn = c; }
	public void setGhostsSpawn(Case c){	_ghostsSpawn = c; }
	public void setParent(Labyrinth laby){ _parent = laby; }
	public void setScoreBoard(ScoreBoard sb){ _scoreBoard = sb; }
	
	/*
	 * Case and line adding/removing
	 */
	
	public void addLine() {
		_grid.add(new Line(this));	
	}
	
	public void removeLine(int line){
		_grid.remove(line);
	}
	
	public void addCase(int column, int line){
		_grid.get(line).add(new Case(column*Case.SIZE, line*Case.SIZE, false, _grid.get(line)));
	}
	
	public void removeCase(int column, int line){
		_grid.get(line).remove(column);
	}

	/**
	 * Finalizes the Map when it is loaded. 
	 */
	public void alignCase(){
		for (int line = 0; line<_gridSize; line++){
			
			_grid.get(line).setParent(this);
			
			for (int column = 0; column<_gridSize; column++){
				
				Case c = getCase(column, line);
				
				c.setPosX(column * Case.SIZE);
				c.setPosY(line * Case.SIZE);
				
				c.setParent(_grid.get(line));
				
				if(c == LoadMapBuffer.ghostSpawn){
					_ghostsSpawn = c;
					_parent.createGhosts(column, line, _nbGhosts);
				}
				else if(c == LoadMapBuffer.fruitspawn){
					_fruitSpawn = c;
					c.setObject(new Fruit(column, line, 50, _fruitTimer));
				}
				else if(c == LoadMapBuffer.pacmanSpawn){
					_playerSpawn = c;
					_parent.createPlayer(column, line);
				}
				
				else if(LoadMapBuffer.superPos.contains(c)){
					c.setObject(new SuperPacGum(column, line, 50));
					_parent.incrGum();
				}
				
				else if(line > 0 && line < _gridSize - 1 && column > 0 && column < _gridSize - 1 && c.isCrossable()){
					c.setObject(new PacGum(column, line, 10));
					_parent.incrGum();
				}
			
			}
		}
	}


	public void print() {
		System.out.println(toString());
	}
	
	public String toString(){
		String str = "";
		for(Line line : _grid)
			str+= line.toString()+"\n";
		
		return str;
	}

	
}
