package xml;

import game.model.Case;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;

import xml.model.CaseConverter;
import xml.model.Line;
import xml.model.Map;
import xml.model.RegisteredScore;
import xml.model.ScoreBoard;
import xml.model.XmlSerializableObject;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

import common.model.DefaultString;

public class XmlSerialization {

	private XStream xstream;
	
	private static XmlSerialization INSTANCE;
	
	private XmlSerialization(){
		xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("_-","_")));
		makeAliases();
		INSTANCE = this;
	}
	
	public static XmlSerialization getInstance(){
		if(INSTANCE == null)
			INSTANCE = new XmlSerialization();
				
		return INSTANCE;
	}

	public void serialize(XmlSerializableObject object, File file){
		
		String xmlString = xstream.toXML(object);
		
		try{
			FileWriter fileWriter = null;
			fileWriter = new FileWriter(file);
			fileWriter.write(xmlString);
			fileWriter.close();
		}catch(Exception e){
			System.out.println("Cannot save file " + e.getMessage());
		}
		
	}
	
	
	public XmlSerializableObject deserialize(FileInputStream fileStream, String fileName){
		XmlSerializableObject object = (XmlSerializableObject) xstream.fromXML(fileStream);
		object.setFileName(fileName);
		return object;
	}
	
	
	private void makeAliases() {

		xstream.alias("ScoreBoard", ScoreBoard.class);
		xstream.alias("RegisteredScore", RegisteredScore.class);
		xstream.alias("Map", Map.class);
		xstream.alias("Line", Line.class);
		xstream.alias("Case", Case.class);
		
		xstream.aliasField("Score", RegisteredScore.class, "_score");
		xstream.aliasField("Player", RegisteredScore.class, "_name");
		
		xstream.aliasField("Name", Map.class, "_name");
		xstream.aliasField("Size", Map.class, "_gridSize");
		xstream.aliasField("Ghosts", Map.class, "_nbGhosts");
		xstream.aliasField("FruitTimer", Map.class, "_fruitTimer");
		xstream.aliasField("SuperTimer", Map.class, "_superTimer");
		xstream.aliasField("Grid", Map.class, "_grid");
		
		xstream.addImplicitCollection(Line.class, "_content", Case.class);
		
		xstream.registerConverter(new CaseConverter());
		
	}
	
}
