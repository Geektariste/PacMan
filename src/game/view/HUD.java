package game.view;

import common.model.Texture;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class HUD extends GridPane{

	private static final String TEXT_SCORE = "Score : ";
	
	private Text tSCORE;
	private Text tRunCpt;
	
	private HBox _lives;

	public HUD(){

        tSCORE = initText(TEXT_SCORE + GameRootPane.getInstance().getLaby().getScore(), 1, 2, Color.BLUE, 20);
        
        tRunCpt = initText("", 5, 5, Color.RED, 50);	    
	
        _lives = new HBox();
        for(int i = 0; i < GameRootPane.getInstance().getLaby().getPlayer().getLife(); i++){
        	_lives.getChildren().add(new ImageView(Texture.TEXTURE_PACMAN_SPAWN.getTexture()));
        }
        
        this.add(_lives, 1, 1);
	}
	

	private Text initText(String text, int x, int y, Color c, int fontSize){
		Text res = new Text(text);
		res.setFill(c);
		res.setFont(new Font(fontSize));
		
		this.add(res, x, y);
		
		return res;
	}
	
	public void updateScore(){
		tSCORE.setText(TEXT_SCORE + GameRootPane.getInstance().getLaby().getScore());
	}
	
	public void updatePrerunningDisplay(String value){
		tRunCpt.setText(value);		
	}
	
	public void decrLife(){
		_lives.getChildren().remove(0);
	}
}
