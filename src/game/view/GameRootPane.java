package game.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import common.MainWindow;
import common.model.DefaultString;
import common.model.Texture;
import common.view.AppWithMenuPane;
import game.controller.AnimationHandler;
import game.controller.KeyListenerGame;
import game.model.Case;
import game.model.Fruit;
import game.model.GameState;
import game.model.Ghost;
import game.model.Labyrinth;
import game.model.abstract_model.Dynamic;
import game.model.abstract_model.Static;
import javafx.animation.AnimationTimer;
import javafx.beans.property.ObjectProperty;
import javafx.scene.Group;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import leaderboard.view.HiScorePane;
import leaderboard.view.LeaderboardRootPane;
import xml.XmlSerialization;
import xml.model.Map;
import xml.model.ScoreBoard;
/**
 *	
 *	The root pane of the game itself. It is composed by canvas layers to separate
 *	background, statics and dynamics.
 *
 */

public class GameRootPane extends AppWithMenuPane{
	
	/*
	 * Game attributes
	 */
	
	private static GameRootPane INSTANCE = null;

	// Display
	private AnchorPane _anchor;
	private Group _gameUI;
	private HUD _HUD;
	
	// ImageViews for dynamics
	private ImageView _player_pane;

	private ArrayList<ImageView> _background_panes_list;
	private HashMap<Static, ImageView> _static_panes_map;
	private HashMap<Dynamic, ImageView> _dynamic_panes_list;
	
	private int preRunningState = 3;
	private int superPacCountdown;
	
	/*
	 * Others
	 */
	
	private Labyrinth _laby;
	private AnimationTimer mainLoop;
	private GameState _gameState;
	
	public GameRootPane(Labyrinth laby){
		
		super();
        
        INSTANCE = this;

		_laby = laby;
		
		/*
		 * View
		 */
		
		_anchor = new AnchorPane();
		
		_menu = new GameMenuPane();
        _gameUI = new Group();    
        _HUD = new HUD();
        
        /*
         * LabyView
         */
        
        _player_pane = new ImageView();
        _player_pane.imageProperty().bind(_laby.getPlayer().getImageProperty());

        _background_panes_list = new ArrayList<ImageView>();
        _static_panes_map = new HashMap<Static, ImageView>();
        _dynamic_panes_list = new HashMap<Dynamic, ImageView>();

        initBackground();
        initDynamics();
        initStatics();

        /*
         * Make layers
         */
        
        AnchorPane.setTopAnchor(_HUD, 0.0);
        AnchorPane.setLeftAnchor(_HUD, 0.0);
        AnchorPane.setBottomAnchor(_HUD, 0.0);
        AnchorPane.setRightAnchor(_HUD, 0.0);
        
        AnchorPane.setTopAnchor(_gameUI, 0.0);
        AnchorPane.setLeftAnchor(_gameUI, 150.0);
        AnchorPane.setBottomAnchor(_gameUI, 0.0);
        AnchorPane.setRightAnchor(_gameUI, 0.0);
	    
        this.getChildren().add(_anchor);
        _anchor.getChildren().add(_gameUI);
        _anchor.getChildren().add(_HUD);
	    
        _gameUI.getChildren().addAll(_background_panes_list);
        _gameUI.getChildren().addAll(_static_panes_map.values());
        _gameUI.getChildren().addAll(_dynamic_panes_list.values());
        
        _gameUI.getChildren().add(_player_pane);
	            
        // Set the PacMan KeyListener
        this.setOnKeyPressed(new KeyListenerGame( this ));
        
		_gameState = GameState.COUNTDOWN;
		moveDynamics();
		movePlayer();
		
		mainLoop = new AnimationHandler(this);
        mainLoop.start();
	}

	
	public void initBackground(){

		Map grid = _laby.getMap();
		Case cell = null;

		for (int line = 0; line < _laby.getGridSize(); line++) {
			for (int column = 0; column < _laby.getGridSize(); column++) {
				
				cell = grid.getCase(column, line);
				ImageView tmp = new ImageView(cell.getImage());
				tmp.setTranslateX(column * Case.SIZE);
				tmp.setTranslateY(line * Case.SIZE);
				
				_background_panes_list.add(tmp);
			}
		}
	}
	
	public void initStatics(){
		
		for(int line = 0; line < _laby.getGridSize(); line++)
			for(int column = 0; column < _laby.getGridSize(); column++){
				
				Static s = _laby.getMap().getCase(column, line).getObject();
				
				if(s != null){
					
					ImageView tmp = new ImageView();
					tmp.imageProperty().bind(s.getImageProperty());
					
					tmp.setTranslateX(s.getPosX());
					tmp.setTranslateY(s.getPosY());
					
					_static_panes_map.put(s, tmp);
					if(s instanceof Fruit)
						tmp.visibleProperty().bind(((Fruit) s).getSpawnedProperty());
				}
			}
	}
	
	public void initDynamics(){
		
		for(Dynamic dyna : _laby.getDynamics()){
			
			ImageView tmp = new ImageView();
			tmp.imageProperty().bind(dyna.getImageProperty());
			
        	_dynamic_panes_list.put(dyna, tmp);
		}
		
	}

	
	public void moveDynamics(){
		ArrayList<Dynamic> dynamics = _laby.getDynamics();
		
		for(Dynamic dyna : _dynamic_panes_list.keySet()){
			
			ImageView tmp_pane = _dynamic_panes_list.get(dyna);
			
			tmp_pane.setTranslateX(dyna.getPosX());
			tmp_pane.setTranslateY(dyna.getPosY());
		}
	}
	
	public void movePlayer(){
		
		ObjectProperty<Image> textureProperty = _laby.getPlayer().getImageProperty();
		
		_player_pane.setTranslateX(_laby.getPlayer().getPosX());
		_player_pane.setTranslateY(_laby.getPlayer().getPosY());
		
		switch(_laby.getPlayer().getMoveState().getCurrentState()){
		case LEFT:
			textureProperty.set(Texture.TEXTURE_PACMAN_LEFT.getTexture());
			break;
		case RIGHT:
			textureProperty.set(Texture.TEXTURE_PACMAN_RIGHT.getTexture());
			break;
		case DOWN:
			textureProperty.set(Texture.TEXTURE_PACMAN_DOWN.getTexture());
			break;
		case UP:
			textureProperty.set(Texture.TEXTURE_PACMAN_UP.getTexture());
			break;
		}
		
	}
	
	public void updateAnimation(){
		switch(_gameState){
		case SUPERPAC:
			
		case RUNNING:
			_laby.getPlayer().move(_laby);
			
			for (Dynamic d : _laby.getDynamics()){
				d.move(_laby);
			}
			
			moveDynamics();
			movePlayer();
			
			if(_laby.getNbGum() == 0)
				_gameState = GameState.WIN;
			
			break;
			
		case COUNTDOWN:
			if(_laby.getPlayer().getLife() == 0)
				_gameState = GameState.LOSE;
			break;
			
		case WIN:
			submitScore();
			break;
			
		case LOSE:
			//tLose.setVisible(true);
			submitScore();
			break;
		}
		
		_HUD.updateScore();
		
	}
	
	public void preRunningDisplay(){
		if(preRunningState > 0){
			
			_HUD.updatePrerunningDisplay(String.valueOf(preRunningState));
			preRunningState--;
			
		}
		else{
			_HUD.updatePrerunningDisplay("");
			_gameState = GameState.RUNNING;
		}
	}
	
	@Override
	public void openCloseMenu(){
		if(!_opened_menu){
			this.getChildren().add(_menu);
			mainLoop.stop();
		}
		else{
			this.getChildren().remove(_menu);
			mainLoop.start();
		}
		_opened_menu = !_opened_menu;
	}	
	
	
	public Labyrinth getLaby(){
		return _laby;
	}

	public static GameRootPane getInstance(){
		return INSTANCE;
	}
	
	/**
	 * Method launched during to enter the countdown state.
	 */
	public void countDown(){
		if(_laby.getPlayer().getLife() > 0){
			_HUD.decrLife();
			_laby.resetPosition();
			_gameState = GameState.COUNTDOWN;
			preRunningState = 3;
		}
		else
			_gameState = GameState.LOSE;
	}
	
	public void stop(){
		mainLoop.stop();
	}
	
	public void run(){
		_gameState = GameState.RUNNING;
	}
	
	public GameState getGameState(){
		return _gameState;
	}
	
	public void setSuper(){
		superPacCountdown = _laby.getSuperTimer();
		_gameState = GameState.SUPERPAC;
		
		for(Dynamic d : _laby.getDynamics()){
			Ghost g = (Ghost)d;
			if(! g.isDead() && !g.isWaiting() )
				g.setStateVulnerable();
		}
	}
	
	/*
	 * Handles the end of the SuperPac state
	 */
	public void endSuper(){
		superPacCountdown--;
		if(superPacCountdown == 0){
			_gameState = GameState.RUNNING;
			
			for(Dynamic d : _laby.getDynamics()){
				Ghost g = (Ghost)d;
				if(! g.isDead() )
					g.setStateNormal();
			}
			
		}
	}

	public void heyImDeadYouKnow(Static s) {
		_static_panes_map.get(s).setVisible(false);
	}

	public void submitScore(){
		stop();
		String fileName = DefaultString.SCOREBOARDS+_laby.getMap().getName()+DefaultString.SCORE_EXTENSION;
		File board = new File(fileName);
		try {
			ScoreBoard sb = (ScoreBoard)XmlSerialization.getInstance().deserialize(new FileInputStream(board), board.getName());
			if( _laby.getScore() > sb.getMinimum() || !sb.isFull()){
				MainWindow.getInstance().switchPane(new HiScorePane(sb));
			}
			else{
				MainWindow.getInstance().switchPane(new LeaderboardRootPane(board));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
