package game.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import common.MainWindow;
import common.exception.InvalidFormatException;
import common.exception.InvalidStructureException;
import common.model.DefaultString;
import common.view.MainMenuPane;
import game.model.Labyrinth;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import xml.XmlSerialization;
import xml.model.Map;
import xml.model.LoadMapBuffer;

public class MapSelectionPane extends BorderPane{
	
	private ScrollPane _scroll;
	private VBox _content;
	
	public MapSelectionPane(){
		
		this.getStyleClass().add("map-selection-pane");
		
		_scroll = new ScrollPane();
		_content = new VBox();
		
		_scroll.setContent(_content);
		
		Text title = new Text("Select map");
		title.setId("title");
		
		this.setTop(title);
		this.setCenter(_scroll);
		
		loadMaps();		
		
		Button returnButton = new Button("Back to main menu");
		returnButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override public void handle(ActionEvent e) {
		        MainWindow.getInstance().switchPane(new MainMenuPane());
		    }
		});
		
		this.setBottom(returnButton);
	}
	
	private void loadMaps(){
		File[] maps = new File(DefaultString.MAPS).listFiles();

		for (File map : maps)
			if (map.getName().endsWith(DefaultString.MAP_EXTENSION)) {
				try {
					LoadMapBuffer.clear();
					Map loaded_map = (Map) XmlSerialization.getInstance().deserialize(
							new FileInputStream(map),	
							map.getName());
					//loaded_map.alignCase();
					//loaded_map.print();
					
					_content.getChildren().add(	new MapDisplayPane(loaded_map));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
	}
	
	private class MapDisplayPane extends BorderPane{
		
		private Map _map;
		private Labyrinth _loadedMap;
		
		public MapDisplayPane(Map map){
			
			_map = map;
			
			try {
				_loadedMap = new Labyrinth(_map.getName(), _map);
				this.setRight(new Text("IMAGE"));
				
				this.setOnMouseClicked(new EventHandler<MouseEvent>(){
					@Override
					public void handle(MouseEvent arg0) {
						MainWindow.getInstance().switchPane(new GameRootPane(_loadedMap));
					}
				});
				
			} 
			
			catch (InvalidFormatException | InvalidStructureException e) {
				this.setRight(new Text("Corrupted Map"));
			}
			
			this.setLeft(new Text(_map.getName()));
		}
		
	}

}
