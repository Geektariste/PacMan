package game.view;

import common.MainWindow;
import common.view.MainMenuPane;
import common.view.MenuPane;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.control.Label;

public class GameMenuPane extends MenuPane{
	
	public GameMenuPane(){
		
		super();
		
		this.getStyleClass().add("game-menu-pane");
		
		Label tMenu = new Label("Menu");
		tMenu.setFont(new Font(80));
		
		Label tGoToMenu = new Label("Return to main menu");
		tGoToMenu.setFont(new Font(30));
		
		tGoToMenu.setOnMouseClicked(new EventHandler(){
			@Override
			public void handle(Event event) {
				MainWindow.getInstance().switchPane(new MainMenuPane());
			}
		});
		
		_menu.getChildren().addAll(tGoToMenu);
		
		this.setTop(tMenu);
		this.setCenter(_menu);
	
	}
	
}
