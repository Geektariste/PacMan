package game.controller;

import game.model.Direction;
import game.model.MoveState;
import game.model.abstract_model.Dynamic;
import game.view.GameRootPane;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

/**
 * 
 * Fully handles in-game KeyPressed events such as :
 * 
 * 		- Movement with arrows
 * 		- Menu opening with Esc
 *
 */

public class KeyListenerGame implements EventHandler<KeyEvent>{

	GameRootPane _parent;
	
	public KeyListenerGame(GameRootPane parent){
		_parent = parent;
	}
	@Override
	public void handle(KeyEvent ke) {
		// TODO Auto-generated method stub
		switch(ke.getCode()){
		
		/*
		 * Movement
		 */
        case UP:
        	_parent.getLaby().getPlayer().setDirection(Direction.UP); 
        	break;
        case DOWN:
        	_parent.getLaby().getPlayer().setDirection(Direction.DOWN);
        	break;
        case LEFT:
        	_parent.getLaby().getPlayer().setDirection(Direction.LEFT);
        	break;
        	
        case RIGHT:
        	_parent.getLaby().getPlayer().setDirection(Direction.RIGHT);
        	break;
        	
        	
        case SPACE:
        	_parent.run();
        	break;
        /*
         * Menu
         */
        case ESCAPE:
        	_parent.openCloseMenu();
        }
	}

}
