package game.controller;

import game.model.GameState;
import game.model.Ghost;
import game.model.abstract_model.Dynamic;
import game.view.GameRootPane;

import java.util.ArrayList;

import javafx.animation.AnimationTimer;

/**
 * Controller which launches every frame.
 */
public class AnimationHandler extends AnimationTimer{

	private GameRootPane _gameRoot;
	
	// one second in nanoseconds
	private final long ONE_SECOND = 1000000000;
	// used to store the current time to calculate fps
	private long currentTime = 0;
	// used to store the last time to calculate fps
	private long lastTime = 0;
	// fps counter
	private int fps = 0;
	// acumulated difference between current time and last time
	private double delta = 0;	
	
	public int preRunningState = 3;
	
	public AnimationHandler(GameRootPane gameRootPane){
		_gameRoot = gameRootPane;
		lastTime = System.nanoTime();
	}
	
	@Override
	public void handle(long now) {
		// TODO Auto-generated method stub
		currentTime = now;
        fps++;
        delta += currentTime-lastTime;
        
        _gameRoot.updateAnimation();
        
        if(delta > ONE_SECOND){
           
           delta -= ONE_SECOND;
           fps = 0;
           
           if(_gameRoot.getGameState() == GameState.COUNTDOWN){
        	   _gameRoot.preRunningDisplay();
           }          
           
           else if(_gameRoot.getGameState() == GameState.SUPERPAC){
        	   _gameRoot.endSuper();
           }
           
           if( ! _gameRoot.getLaby().getFruit().isSpawned() )
        	   _gameRoot.getLaby().fruitSpawn();
           
           ArrayList<Dynamic> dynamics = _gameRoot.getLaby().getDynamics();
           
           for(Dynamic d : dynamics){
        	   Ghost g = (Ghost)d;
        	   if(g.isWaiting())
        		   g.endWaiting();
           }
        	   
        }
        
        lastTime = currentTime;
	}

}
