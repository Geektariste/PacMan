package game.model;

import common.model.Texture;

import game.model.abstract_model.Static;
import game.view.GameRootPane;
import javafx.scene.image.Image;

/**
 * Represents a PacGum in the game. Extends Static.
 */
public class PacGum extends Static {

	public static int SIZE = 5;

	public PacGum(int pos_x, int pos_y, int points) {

		super(pos_x, pos_y, SIZE, points, Texture.TEXTURE_PACGUM);

	}
	
	/*
	 * Called by SuperPacGum()
	 */
	public PacGum(int pos_x, int pos_y, int size, int points, Texture texture) {

		super(pos_x, pos_y, size, points, texture);

	}

	@Override
	public int be_eaten() {
		super.be_eaten();
		GameRootPane.getInstance().getLaby().decrGum();
		return _points;
	}

}