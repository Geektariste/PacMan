package game.model;

/**
 * Enum defining the different states of directions set.
 */
public enum Direction {

	UP(1),
	DOWN(2),
	LEFT(3),
	RIGHT(4);
	
	private final int _dir;
	
	private Direction(int dir){
		_dir = dir;
	}
	
}
