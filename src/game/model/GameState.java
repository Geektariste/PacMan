package game.model;

/**
 * Defines the different states of the game.
 */
public enum GameState {
	
	RUNNING(1),
	COUNTDOWN(2),
	WIN(3),
	LOSE(4),
	SUPERPAC(5);
	
	private final int _state;
	
	private GameState(int state){	
		_state = state;
	}

	
}
