package game.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import common.model.DefaultString;

public class FileBuilder {

	/*
	 * Creates / Replaces the file named fileName
	 * Writting the char[][] in it.
	 * Used to save a map using the editor.
	 */
	public static void buildFile(String fileName, char[][] tab, int fruitTimer, int superTimer){
		try{
			PrintWriter pw = new PrintWriter (new BufferedWriter (new FileWriter (DefaultString.MAPS + fileName)));
			pw.println(tab.length);
			for(int i = 0; i < tab.length; i++){
				for(int j = 0; j < tab[i].length; j++)
					pw.print(tab[i][j]);
				pw.println();
				pw.flush();
			}
			pw.print("F:" + fruitTimer);
			pw.print("S:" + superTimer);
			pw.flush();
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	/*
	 * Checks different things:
	 *  - first line contains the width
	 *  - Number of lines = number of columns
	 *  - only 1 Pacman spawn
	 *  - only 1 Ghosts spawn
	 *  - only 1 Fruit spawn
	 */
	public static boolean isFileOK(String filePath){
		/*
		try{
			InputStream ips=new FileInputStream(filePath); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String fileRead = br.readLine();
			
			int width = 0;
			
			// Verification de la pr�sence de la taille
			Pattern p_correctWidth = Pattern.compile("^[0-9]+$");
			Matcher m_correctWidth = p_correctWidth.matcher(fileRead);
			if(! m_correctWidth.find())
				return false;
			else
				width = Integer.parseInt(fileRead);
			
			Pattern p_correctLine = Pattern.compile("^[X|G|P|F|V|S]{" + width + "}$");
			Matcher m_correctLine;
			
			// V�rification de la coh�rence du labyrinthe
			int nb_P = 0;
			int nb_G = 0;
			int nb_F = 0;
			int nb_S = 0;
			for(int i = 0; i < width; i++){
				fileRead = br.readLine();
				m_correctLine = p_correctLine.matcher(fileRead);
				if( ! m_correctLine.find() )
					return false;
				if(fileRead.indexOf('P') != -1)
					nb_P++;
				if(fileRead.indexOf('G') != -1)
					nb_G++;
				if(fileRead.indexOf('F') != -1)
					nb_F++;
				if(fileRead.indexOf('S') != -1)
					nb_S++;
			}
			
			// V�rification du timer du fruit
			if((fileRead = br.readLine()) != null){
				System.out.println(fileRead);
				Pattern p_correctFruitTimer = Pattern.compile("^F:[0-9]+$");
				Matcher	m_correctFruitTimer = p_correctFruitTimer.matcher(fileRead);
				if( ! m_correctFruitTimer.find() )
					return false;
			}
			
			// Verification du timer du Super Pac-Man
			if((fileRead = br.readLine()) != null){
				System.out.println(fileRead);
				Pattern p_correctSuperTimer = Pattern.compile("^S:[0-9]+$");
				Matcher m_correctSuperTimer = p_correctSuperTimer.matcher(fileRead);
				if(! m_correctSuperTimer.find() )
					return false;
			}
			
			br.close();
			ipsr.close();
			ips.close();
			
			return (nb_P == 1 && nb_G == 1 && nb_F == 1 && nb_S > 0);

		}
		catch(Exception e){System.out.println("Crash");}
		
		
		return false;
		*/
		return true;
	}
	
}
