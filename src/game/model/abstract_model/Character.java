package game.model.abstract_model;

import common.model.Texture;

/**
 * Represents a Dynamic that can eat an Actor. Extends Dynamic.
 */
public abstract class Character extends Dynamic {

	public Character(int pos_x, 
					 int pos_y, 
					 int size,
					 int points,
					 int speed,
					 Texture image) {

		super(pos_x, pos_y, size, points, speed, image);

	}

	/**
	 * Launched when the Character eats an Actor.
	 * @param a The actor which is eaten
	 */
	public abstract void eat(Actor a);
	
}
