package game.model.abstract_model;

import game.model.Case;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;

import common.model.Texture;

/**
 * Defines what an Actor is (size, positions, ...).
 */
public abstract class Actor {

	protected int _pos_x;
	protected int _pos_y;
	protected int _size;
	protected int _points;
	
	/*
	 * Loaded in subclasses
	 */
	protected Texture _baseImage; 
	private ObjectProperty<Image> _currentImage;

	public Actor(int pos_x, 
				 int pos_y, 
				 int size,
				 int points,
				 Texture image) {

		_size = size;
		_points = points;
		_baseImage = image;
		
		calcultatePositionFromCaseIndex(pos_x, pos_y);
		
		_currentImage = new SimpleObjectProperty<Image>(_baseImage.getTexture());
	}
	
	/**
	 * Method launched when an actor is eaten.
	 * 
	 * @return points earned
	 */
	public abstract int be_eaten();
	
	
	public int getPosX(){
		return _pos_x;
	}
	
	public int getPosY(){
		return _pos_y;
	}
	
	public int getSize(){
		return _size;
	}
	
	public ObjectProperty<Image> getImageProperty(){
		return _currentImage;
	}
	
	private void calcultatePositionFromCaseIndex(int index_x, int index_y){
		_pos_x = index_x*Case.SIZE + (Case.SIZE - _size) / 2;
		_pos_y = index_y*Case.SIZE + (Case.SIZE - _size) / 2;
	}

}
