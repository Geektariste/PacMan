package game.model.abstract_model;

import game.view.GameRootPane;

import common.model.Texture;

/**
 * Represents a Static object. Extends Actor.
 */
public abstract class Static extends Actor {

	public Static(int pos_x, 
				  int pos_y, 
				  int size, 
				  int points,
				  Texture image) {

		super(pos_x, pos_y, size, points, image);

	}
	
	/**
	 * Launched when the Actor is being eaten.
	 */
	@Override
	public int be_eaten(){
		GameRootPane.getInstance().heyImDeadYouKnow(this);
				
		return _points;
	}

}
