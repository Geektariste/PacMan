package game.model.abstract_model;

import common.model.Texture;
import game.model.Case;
import game.model.Direction;
import game.model.Labyrinth;
import game.model.MoveState;
import game.model.PacMan;
import javafx.geometry.Rectangle2D;
import xml.model.Map;

/**
 * Represents a Dynamic Object in the game. Extends Actor.
 */
public abstract class Dynamic extends Actor {

	protected int _speed;
	protected MoveState _movestate;
	
	public Dynamic(int pos_x, 
				   int pos_y, 
				   int size, 
				   int points, 
				   int speed,
				   Texture image) {
		
		super(pos_x, pos_y, size, points, image);
		_speed = speed;
		_movestate = new MoveState();
	}
	
	

	/**
	 * Tests if the next movement set is available.
	 * 
	 * @param laby Labyrinth
	 * @return True if the movement is available. False otherwise.
	 */
	public boolean available_movement(Labyrinth laby){

		Map grid = laby.getMap();

		int column = _pos_x / Case.SIZE;
		int line = _pos_y / Case.SIZE;
		
		switch(_movestate.getNextState()){
		case DOWN:
			line++;
			break;
		case UP:
			line--;
			break;
		case LEFT:
			column--;
			break;
		case RIGHT:
			column++;
			break;
		}
		
		if(column >= 0 && line >= 0 && column < laby.getGridSize() && line < laby.getGridSize() 
				&& grid.getCase(column,line).isCrossable()){
			
			Rectangle2D r = new Rectangle2D( 	(_pos_x / Case.SIZE) * Case.SIZE + (Case.SIZE / 2) - 4, 
												(_pos_y / Case.SIZE) * Case.SIZE + (Case.SIZE / 2) - 4, 8, 8);
			
			return r.contains(_pos_x + (_size / 2), _pos_y + (_size / 2) );
		}
		
		else
			return false;
	}
	
	
	/**
	 * Replaces the Dynamic object in the center of the slot.
	 */
	public void replacement(){
		switch(_movestate.getCurrentState()){
		case DOWN:
		case UP:
			_pos_x = (int)( _pos_x / Case.SIZE ) * Case.SIZE + ( ( Case.SIZE - _size ) / 2 );
			break;
		case LEFT:
		case RIGHT:
			_pos_y = (int)( _pos_y / Case.SIZE ) * Case.SIZE + ( ( Case.SIZE - _size ) / 2 );
			break;
		}
	}
	
	/**
	 * Update the position of the Dynamic object.
	 * 
	 * @param laby The Labyrinth
	 */
	public void updatePosition(Labyrinth laby){
		boolean moved = true;
		switch(_movestate.getCurrentState()){
		
		case RIGHT:
			if( ( _pos_x + _speed + _size + (Case.SIZE - _size) / 2 ) / Case.SIZE ==laby.getGridSize()){
				_pos_x += _speed;
				if( _pos_x > laby.getGridSize() * Case.SIZE )
					_pos_x = 0;
			}
			else if( laby.getMap().getCase(( _pos_x + _speed + _size + (Case.SIZE - _size) / 2 ) / Case.SIZE, _pos_y / Case.SIZE).isCrossable()){
				_pos_x += _speed;
			}
			else
				moved = false;
			break;
		case LEFT:
			if( laby.getMap().getCase(( _pos_x - _speed - (Case.SIZE - _size) / 2) / Case.SIZE, _pos_y / Case.SIZE).isCrossable() )
				_pos_x -= _speed;
			else
				moved = false;
			if(_pos_x < 0)
				_pos_x = ( laby.getGridSize() ) * Case.SIZE - 1;
			break;
		case UP:
			if( laby.getMap().getCase(_pos_x / Case.SIZE, (_pos_y - _speed - (Case.SIZE - _size) / 2) / Case.SIZE).isCrossable() )
				_pos_y -= _speed;
			else
				moved = false;
			if(_pos_y < 0)
				_pos_y = ( laby.getGridSize() ) * Case.SIZE - 1;
			break;
		case DOWN:
			if( ( _pos_y + _speed + _size + (Case.SIZE - _size) / 2 ) / Case.SIZE >= laby.getGridSize()){
				_pos_y += _speed;
				if( _pos_y > laby.getGridSize() * Case.SIZE )
					_pos_y = 0;
			}
			else if(laby.getMap().getCase(_pos_x / Case.SIZE, ( _pos_y + _speed + _size + (Case.SIZE - _size) / 2 ) / Case.SIZE).isCrossable() )
				_pos_y += _speed;
			else
				moved = false;
			break;
		}
		
		if( ! moved ){
			replacement();
			changeDirection();
		}
	}
	
	/**
	 * Method launched each frame.
	 * 
	 * @param laby The Labyrinth
	 */
	public void move(Labyrinth laby){	
		
		if(available_movement(laby)){
			changeDirection();
			replacement();
		}
		
		updatePosition(laby);
		
	}
	
	/**
	 * Replaces the current direction by the next one.
	 */
	public void changeDirection(){
		_movestate.setCurrentState(_movestate.getNextState());
	}
	
	/**
	 * Sets the next direction.
	 * 
	 * @param direction The direction to set
	 */
	public void setDirection(Direction direction){
		_movestate.setNextState(direction);
	}
	
	public MoveState getMoveState(){
		return _movestate;
	}

	public void setPosX(int x){
		_pos_x = x;
	}
	
	public void setPosY(int y){
		_pos_y = y;
	}
	
}
