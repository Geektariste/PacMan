package game.model;

import game.model.abstract_model.Dynamic;
import game.view.GameRootPane;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;

import common.model.Texture;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import xml.model.Map;

/**
 * Represents a ghost in the game. Extends Dynamic.
 */
public class Ghost extends Dynamic{
	
	public static int SIZE = 20;
	public static int WAITING_TIME = 3;
	
	private Random rand;
	private int cpt;
	
	private GhostState _state;
	
	private Stack<Case> pathToFollow;
	
	private int _nominalSpeed;
	
	private boolean recentChange;
	private boolean vulnerable;
	
	private int isWaitingSince;
	
	private static int _diversity = 0;
	
	/*
	 * \!/ speed must be >= 2
	 */
	public Ghost(int pos_x, 
				  int pos_y, 
				  int points,
				  int speed) {
		
		super(pos_x, pos_y, SIZE, points, speed, getTexture());
		
		rand = new Random();
		cpt = 0;
		recentChange = false;
		
		_state = GhostState.RANDOM;

		_nominalSpeed = speed;
		
		isWaitingSince = 0;

		setStateNormal();
	}

	@Override
	public int be_eaten() {
		// TODO Auto-generated method stub
		setStateDead();
		return _points;
	}
	
	/**
	 * Handles directions taken randomly.
	 * 
	 * @param laby Labyrinth
	 */
	public void randomMovement(Labyrinth laby){
		if(recentChange){
			if(cpt++ > Case.SIZE / _speed){
				recentChange = false;
				cpt= 0;
			}
		}
		else{
			ArrayList<Direction> availableDir = new ArrayList<Direction>();
			
			int x = _pos_x / Case.SIZE;
			int y = _pos_y / Case.SIZE;
			
			Map grid = laby.getMap();
			Direction currentState = _movestate.getCurrentState();
			
			if( ! (x <= 0 || x >= laby.getGridSize() - 1 || y <= 0 || y >= laby.getGridSize() - 1) ){
				
				if(currentState != Direction.UP)
					if(grid.getCase(x, y + 1).isCrossable())
						availableDir.add(Direction.DOWN);
				
				if(currentState != Direction.DOWN)
					if(grid.getCase(x, y - 1).isCrossable())
						availableDir.add(Direction.UP);
				
				if(currentState != Direction.LEFT)
					if(grid.getCase(x + 1, y).isCrossable())
						availableDir.add(Direction.RIGHT);
				
				if(currentState != Direction.RIGHT)
					if(grid.getCase(x - 1, y).isCrossable())
						availableDir.add(Direction.LEFT);
				
				Direction new_dir = availableDir.get( rand.nextInt(availableDir.size() ) );
				if(new_dir != currentState){
					setDirection( new_dir );
					recentChange = true;
				}
			}
		}
	}
	
	/**
	 * Handles actions that need to be done when a ghost is in collision with PacMan.
	 * 
	 * @param player PacMan
	 */
	public void collisionDetection(PacMan player){
		if(_state != GhostState.DEAD){
			Rectangle2D r = new Rectangle2D(_pos_x, _pos_y, _size, _size);
			Rectangle2D playerRect = new Rectangle2D(player.getPosX(), player.getPosY(), player.getSize(), player.getSize());
			if( r.intersects(playerRect) ){
				switch(GameRootPane.getInstance().getGameState()){
				case RUNNING:	
					player.be_eaten();
					break;
				case SUPERPAC:
					if(! isDead() && ! isWaiting()){
						if(vulnerable){
							player.eat(this);
							pathToFollow = null;
							_state = GhostState.DEAD;
						}
						else
							player.be_eaten();
					}
					break;
					}
						
			}
		}
	}
	
	/**
	 * Makes the Ghost follow PacMan if they are close enough ( less than laby size / 4 ).
	 * 
	 * @param p PacMan
	 */
	public void chasingModeDetection(PacMan p){
		int playerX = p.getPosX() / Case.SIZE;
		int playerY = p.getPosY() / Case.SIZE;
		int x = _pos_x / Case.SIZE;
		int y = _pos_y / Case.SIZE;
		int width = GameRootPane.getInstance().getLaby().getGridSize();
		int t =  Math.min(
				Math.min(
						Math.abs(x - playerX),
						Math.abs(x - (playerX - width)) 
						),
					Math.abs(x - (playerX + width))
					)
				+ 
				Math.min(
					Math.min(
						Math.abs(y - playerY), 
						Math.abs(y - (playerY - width))
						),
					Math.abs(y - (playerY + width))
						);

		if(! vulnerable )
			_state = (t < (width / 4) + 1 ? GhostState.CHASING : GhostState.RANDOM);
		else
			_state = GhostState.RANDOM;
	}
	
	/**
	 * Makes the ghost follow the path pathToFollow.
	 * 
	 * @param laby The Labyrinth
	 */
	public void followPath(Labyrinth laby){
		int x = _pos_x / Case.SIZE;
		int y = _pos_y / Case.SIZE;
		
		int width = laby.getGridSize();
		Map grid = laby.getMap();

		if(x >= 0 && x < width && y >= 0 && y < width && grid.getCase(x, y) == pathToFollow.peek()){
			pathToFollow.pop();
		}
			
		if(! pathToFollow.isEmpty() ){
			Case tmp = pathToFollow.peek();
			int tmpX = tmp.getPosX() / Case.SIZE;
			int tmpY = tmp.getPosY() / Case.SIZE;
			
			if(x != 0 && x != width - 1){
				if(x < tmpX)
					setDirection(Direction.RIGHT);	
				else if(x > tmpX)
					setDirection(Direction.LEFT);
			}
			if(y != 0 && y != width - 1){
				if(y < tmpY)
					setDirection(Direction.DOWN);
				else if (y > tmpY)
					setDirection(Direction.UP);
			}
		
		}
		else{
			pathToFollow = null;
			if( isDead() )
				setStateWaiting();
			else
				setStateNormal();
			//_state = GhostState.RANDOM;
		}
		
		
	}
	
	public void move(Labyrinth laby){
		
		super.move(laby);
		collisionDetection(laby.getPlayer());

		switch(_state){
		case CHASING:
			if( _pos_x >= 0 && _pos_y >= 0 && _pos_x < laby.getGridSize()*Case.SIZE && _pos_y < laby.getGridSize()*Case.SIZE){
				chasingModeDetection(laby.getPlayer());
				pathToFollow = laby.pathFinding(laby.getMap().getCase(_pos_x / Case.SIZE, _pos_y / Case.SIZE), 
								laby.getMap().getCase(laby.getPlayer().getPosX() / Case.SIZE, laby.getPlayer().getPosY() / Case.SIZE));
			}
		case DEAD:
			if(pathToFollow == null){
				pathToFollow = laby.pathFinding(laby.getMap().getCase(_pos_x / Case.SIZE, _pos_y / Case.SIZE), laby.getMap().getGhostsSpawn());
			}

			followPath(laby);
			break;
		case RANDOM:	
			randomMovement(laby);
			chasingModeDetection(laby.getPlayer());
			break;
		}
		
	}
	
	/**
	 * Sets the ghost into a vulnerable state when a superPacGum is eaten.
	 */
	public void setStateVulnerable(){
		_speed = _nominalSpeed - 1;
		vulnerable = true;
		this.getImageProperty().set(Texture.TEXTURE_VULNERABLE.getTexture());
	}
	
	/**
	 * Sets the ghost into a normal state.
	 */
	public void setStateNormal(){
		_speed = _nominalSpeed;
		vulnerable = false;
		this.getImageProperty().set(_baseImage.getTexture());
		_state = GhostState.RANDOM;
	}
	
	/**
	 * Sets the ghost into a dead state when the ghost has been eaten. 
	 */
	public void setStateDead(){
		_speed = _nominalSpeed + 1;
		vulnerable = false;
		this.getImageProperty().set(Texture.TEXTURE_DEAD_LEFT.getTexture());
	}
	
	public boolean isDead(){
		return _state == GhostState.DEAD;
	}
	
	public boolean isWaiting(){
		return _state == GhostState.WAITING;
	}
	
	/**
	 * Sets the waiting state.
	 */
	public void setStateWaiting(){
		_state = GhostState.WAITING;
		vulnerable = false;
		this.getImageProperty().set(Texture.TEXTURE_DEAD_LEFT.getTexture());
	}
	
	/**
	 * Handles the end of waiting time.
	 */
	public void endWaiting(){
		if(isWaitingSince++ == WAITING_TIME){
			isWaitingSince = 0;
			setStateNormal();
		}
	}
	
	public static Texture getTexture(){
		_diversity = ((_diversity++) %4 ) + 1;
		Texture to_load = Enum.valueOf(Texture.class, "TEXTURE_GHOST_"+_diversity);
		return to_load;
	}
	
}
