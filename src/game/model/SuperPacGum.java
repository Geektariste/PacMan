package game.model;

import common.model.Texture;

import game.model.abstract_model.Static;
import game.view.GameRootPane;
import javafx.scene.image.Image;

/**
 * Defines a Super PacGum in the game. Extends PacGum.
 */
public class SuperPacGum extends PacGum {

	public static int SIZE = 10;

	public SuperPacGum(int pos_x, int pos_y, int points){
		super (pos_x, pos_y, SIZE, points, Texture.TEXTURE_SUPERGUM);
	}
	
	@Override
	public int be_eaten(){
		super.be_eaten();
		GameRootPane.getInstance().setSuper();
		return _points;
	}
}
