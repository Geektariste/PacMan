package game.model;

import common.model.Texture;

import game.model.abstract_model.Static;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.image.Image;

/**
 * Represents a Fruit in the game. Extends Static.
 */
public class Fruit extends Static {

	public static int SIZE = 8;
	
	private int _spawnTime;
	private SimpleBooleanProperty _spawned;
	private int _lastSpawn = 0;

	public Fruit(int pos_x, 
				 int pos_y, 
				 int points, 
				 int spawnTime) {
		
		super(pos_x, pos_y, SIZE, points, Texture.TEXTURE_FRUIT);
		_spawnTime = spawnTime;
		_spawned = new SimpleBooleanProperty(false);
	}

	@Override
	public int be_eaten() {
		if(isSpawned()){
			setSpawned(false);
			_lastSpawn = 0;
			return _points;
		}
		return 0;
	}

	public int getSpawnTime(){
		return _spawnTime;
	}
	
	public boolean isSpawned(){
		return _spawned.get();
	}
	
	public void spawn(){
		_spawned.set(true);
	}
	
	public int incrLastSpawn(){
		_lastSpawn ++;
		return _lastSpawn;
	}
	
	public void setSpawnTime(int time){
		_spawnTime = time;
	}
	
	public SimpleBooleanProperty getSpawnedProperty(){
		return _spawned;
	}
	
	public void resetLastSpawn(){
		_lastSpawn = 0;
	}
	
	public void setSpawned(boolean spawned){
		_spawned.set(spawned);
	}
	
}
