package game.model;

import game.model.abstract_model.Dynamic;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Stack;

import xml.model.Map;

import javafx.scene.image.Image;
import javafx.util.Pair;

import common.exception.InvalidFormatException;
import common.exception.InvalidStructureException;
import common.model.DefaultString;

/**
 * Contains all elements of the game.
 */
public class Labyrinth {
	
	private Map _map;
	
	private ArrayList<Dynamic> _dynamics;

	private PacMan _player;
	
	
	private int _score;
	private int nb_gum;
	
	private int _superTimer;
	
	/*
	 * Images
	 */
	
	private String _theme;
	
	public Labyrinth(String fileName, Map map) throws InvalidFormatException, InvalidStructureException{
		this(fileName, map, false);
	}
	
	public Labyrinth(String fileName, Map map, boolean forceLoad) throws InvalidFormatException, InvalidStructureException{
		nb_gum = 0;
		_dynamics = new ArrayList<Dynamic>();
		_score = 0;
		
		String filePath = DefaultString.MAPS + fileName;
		
		if( ! FileBuilder.isFileOK(filePath) ) 
			throw new InvalidFormatException();
		
		_map = map;
		
		_map.setParent(this);
		_map.alignCase();
		
		if(!forceLoad && !isValidLaby() )
			throw new InvalidStructureException();
		
	}

	public boolean isValidLaby(){
		
		return ( validBounds() && corridorOnly() && noCulDeSac() ) ;
	}
	
	/**
	 * Verifies that each Case has 2 crossable Cases in its neighborhood.
	 * 
	 * @return True if the test passed. False otherwise.
	 */
	public boolean noCulDeSac(){
		for(int i = 1; i < this.getGridSize() - 1; i++){
			for(int j = 1; j < this.getGridSize() - 1; j++){
				if(_map.getCase(i, j).isCrossable()){
					int cpt = 0;
					if(_map.getCase(i - 1, j).isCrossable())
						cpt++;
					if(_map.getCase(i + 1, j).isCrossable())
						cpt++;
					if(_map.getCase(i, j + 1).isCrossable())
						cpt++;
					if(_map.getCase(i, j - 1).isCrossable())
						cpt++;
					if(cpt < 2)
						return false;
				}
			}
		}
		return true;
	}

	public boolean corridorOnly(){
		
		for(int line = 0; line < this.getGridSize() - 2; line++){
			for(int column = 0; column < this.getGridSize() - 2; column++){
				if(_map.getCase(column, line).isCrossable()){
					if(_map.getCase(column, line + 1).isCrossable() && 
							_map.getCase(column + 1, line).isCrossable() && 
							_map.getCase(column + 1, line + 1).isCrossable())
						return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Verifies that when there is a way in a bound, there is a way out on the other side.
	 * @return True if the test passed. False otherwise.
	 */
	public boolean validBounds(){
		// Every corner is not crossable
		
		if(_map.getCase(0, this.getGridSize() - 1).isCrossable() || 
				_map.getCase(0, 0).isCrossable() ||
				_map.getCase(this.getGridSize() - 1, 0).isCrossable() ||
				_map.getCase(this.getGridSize() - 1, this.getGridSize() - 1).isCrossable())
			return false;
		
		// check on bounds
		for(int i = 1; i < this.getGridSize() - 2; i++){
			if(_map.getCase(i,0).isCrossable() && ! _map.getCase(i, this.getGridSize() - 1).isCrossable())
				return false;
			if(_map.getCase(0,i).isCrossable() && ! _map.getCase(this.getGridSize() - 1, i).isCrossable())
				return false;
		}
		return true;
	}
	
	public ArrayList<Dynamic> getDynamics(){
		return _dynamics;
	}

	public String toString(){
		
		StringBuilder str = new StringBuilder();
		for(int line = 0; line<this.getGridSize(); line++){
			for(int column = 0; column<this.getGridSize(); column++)
				str.append(_map.getCase(column, line).toString());
		
			str.append("\n");
		}
		
		return str.toString();
		
	}
	
	/**
	 * Handles the fruit spawn.
	 */
	public void fruitSpawn(){
		if( getFruit().incrLastSpawn() > getFruit().getSpawnTime() ){
			_map.getFruitSpawn().setObject(getFruit());
			getFruit().spawn();
		}
	}
	
	/**
	 * Resets Ghosts and PacMan position on their spawn.
	 */
	public void resetPosition(){
		resetPlayerPosition();
		
		for(Dynamic d : _dynamics){
			resetGhostPosition((Ghost)d);
		}
		
		getFruit().resetLastSpawn();
		getFruit().setSpawned(false);
	}
	
	/**
	 * Resets Ghost position to its spawn.
	 * @param g Ghost which is reset
	 */
	public void resetGhostPosition(Ghost g){

		g.setPosX( (_map.getGhostsSpawn().getPosX() / Case.SIZE ) * Case.SIZE + (Case.SIZE - 25) / 2 );
		g.setPosY( (_map.getGhostsSpawn().getPosY() / Case.SIZE ) * Case.SIZE + (Case.SIZE - 25) / 2 );

		g.setStateNormal();
	}
	
	/**
	 * Reset PacMan position to its spawn.
	 */
	public void resetPlayerPosition(){
		_player.setPosX( (_map.getPlayerSpawn().getPosX() / Case.SIZE ) * Case.SIZE + (Case.SIZE - 20) / 2 ); 
		_player.setPosY( (_map.getPlayerSpawn().getPosY() / Case.SIZE ) * Case.SIZE + (Case.SIZE - 20) / 2 );
		_player.getMoveState().setCurrentState(Direction.LEFT);
		_player.getMoveState().setNextState(Direction.LEFT);
	}
	
	/**
	 * Returns a List which contain the crossable Case in the neighborhood of c.
	 * 
	 * @param c The Case we are looking for its neighborhood
	 * @return The List of these Case
	 */
	public ArrayList<Case> getNeighborsCrossable(Case c){
		ArrayList<Case> res = new ArrayList<Case>();
		int x = c.getPosX() / Case.SIZE;
		int y = c.getPosY() / Case.SIZE;

		if(x > 0 && _map.getCase(x - 1, y).isCrossable())
			res.add(_map.getCase(x - 1, y));

		else if(x == 0)
			res.add(_map.getCase(getGridSize() - 1, y));


		if(x < getGridSize() - 1 && _map.getCase(x + 1, y).isCrossable())
			res.add(_map.getCase(x + 1, y));

		else if(x == getGridSize() - 1)
			res.add(_map.getCase(0, y));
		
		if(y > 0 && _map.getCase(x, y - 1).isCrossable())
			res.add(_map.getCase(x, y - 1));
			
		else if(y == 0)
			res.add(_map.getCase(x, getGridSize() - 1));

		if(y < getGridSize() - 1 && _map.getCase(x, y + 1).isCrossable())
			res.add(_map.getCase(x, y + 1));
			
		else if(y == getGridSize() - 1)
			res.add(_map.getCase(x, 0));
		
		return res;
	}

	/**
	 * Returns a stack which contains a path from start to goal.
	 * 
	 * @param start Case
	 * @param goal Case
	 * @return The path to follow
	 */
	public Stack<Case> pathFinding(Case start, Case goal){
		int goalX = goal.getPosX() / Case.SIZE;
		int goalY = goal.getPosY() / Case.SIZE;
		
		PriorityQueue<Pair<Case, Integer>> frontier = new PriorityQueue<Pair<Case,Integer>>(50, 
				new Comparator< Pair<Case, Integer> >(){
			
			@Override
			public int compare(Pair<Case, Integer> a, Pair<Case, Integer> b){
				return a.getValue().compareTo(b.getValue());
			}
			
		});
		HashMap<Case, Case> came_from = new HashMap<Case, Case>();
		HashMap<Case, Integer> cost_so_far = new HashMap<Case, Integer>();
		
		frontier.add(new Pair<Case, Integer>(start, 0));
		came_from.put(start, null);
		cost_so_far.put(start, 0);
		
		Case current;
		while(! frontier.isEmpty() ){
			current = frontier.poll().getKey();
			if(current == goal)
				break;
			ArrayList<Case> neighbors = getNeighborsCrossable(current);
			int new_cost, priority;
			for(Case next : neighbors){
				new_cost = cost_so_far.get(current) + 1;
				if( ! (cost_so_far.containsKey(next) ) || new_cost < cost_so_far.get(next) ){
					cost_so_far.put(next, new_cost);
					int tmpX = next.getPosX() / Case.SIZE;
					int tmpY = next.getPosY() / Case.SIZE;
					priority = new_cost + 
							Math.min(
								Math.min(
									Math.abs(goalX - tmpX),
									Math.abs(goalX - (tmpX - getGridSize())) 
									),
								Math.abs(goalX - (tmpX + getGridSize()))
								)
							+ 
							Math.min(
								Math.min(
									Math.abs(goalY - tmpY), 
									Math.abs(goalY - (tmpY - getGridSize()))
									),
								Math.abs(goalY - (tmpY + getGridSize()))
									);
					frontier.add(new Pair<Case, Integer>(next, priority));
					came_from.put(next, current);
				}
			}
		}
		
		Case tmp = goal;
		Stack<Case> path = new Stack<Case>();
		path.push(tmp);
		while((tmp = came_from.get(tmp)) != null){
			path.push(tmp);
		}
		return path;
	}
	
	/**
	 * Decrements the number of gum left.
	 */
	public void decrGum(){
		nb_gum--;
	}
	
	/**
	 * Increments the number of gum left.
	 */
	public void incrGum(){
		nb_gum++;
	}
	
	/*
	 * Actors creation
	 */
	
	public void createPlayer(int pos_x, int pos_y){
		_player = new PacMan(pos_x, pos_y, 3, 3);
	}
	

	public void createGhosts(int pos_x, int pos_y, int nbGhosts) {
		for(int i = 0; i < nbGhosts; i++)
			_dynamics.add(new Ghost(pos_x, pos_y, 10, 2));
	}
	
	/*
	 * Getters
	 */
		
	public int getNbGum(){
		return nb_gum;
	}
	
	public int getGridSize() {
		return _map.getGridSize();
	}
	
	public PacMan getPlayer(){
		return _player;
	}
	
	public Fruit getFruit(){
		return (Fruit)_map.getFruitSpawn().getObject();
	}
	
	public int getSuperTimer(){
		return _superTimer;
	}
	
	public Map getMap(){
		return _map;
	}
	
	public int getScore(){
		return _score;
	}
	
	/*
	 * Setters
	 */
	
	public void setSuperTimer(int s){
		_superTimer = s;
	}
	
	public void setMap(Map map){
		_map = map;
	}
	
	public void addScore(int amount){
		_score += amount;
	}

	public void setGridSize(int size) {
		_map.setGridSize(size);
	}

}
