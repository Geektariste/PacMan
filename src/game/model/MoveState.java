package game.model;

/**
 * Contains the current Direction and the next planned Direction.
 */
public class MoveState {
	
	private Direction _currentState;
	private Direction _nextState;
	
	public MoveState(){	
		_currentState = Direction.LEFT;
		_nextState = Direction.LEFT; 
	}
	
	public MoveState(Direction state){
		_currentState = state;
		_nextState = state;
	}	
	
	public Direction getCurrentState(){
		return _currentState;
	}
	
	public void setCurrentState(Direction state){
		_currentState = state;

	}
	
	public Direction getNextState(){
		return _nextState;
	}
	
	public void setNextState(Direction state){
		_nextState = state;
	}

}
