package game.model;

import common.model.Texture;
import game.model.abstract_model.Static;
import javafx.scene.image.Image;
import xml.model.Line;
import xml.model.LoadMapBuffer;

/**
 * Defines the characteristics of a Case (crossable, object contained, ...).
 */
public class Case {

	public static int SIZE = 32;

	private int _pos_x;
	private int _pos_y;
	
	private Static _object;
	private Image _image;

	private boolean _crossable;
	
	private transient Line _parent;

	public Case(int pos_x, 
				int pos_y,
				boolean crossable,
				Line parent) {

		_pos_x = pos_x;
		_pos_y = pos_y;
		_crossable = crossable;
		_object = null;
		_parent = parent;
		
		updateTexture();
	}
	
	/**
	 * Sets the object o in this Case.
	 * 
	 * @param object Object to set in the Case
	 */
	public void setObject(Static object){
		_object = object;
	}
	
	public Static getObject(){
		return _object;
	}
	
	public String toString(){
		
		String str = "";
		
		if(_crossable)
			if(this == LoadMapBuffer.ghostSpawn)
				str = "GS";
			else if(this.getObject() instanceof Fruit)
				str = "FS";
			else if(this == LoadMapBuffer.pacmanSpawn)
				str = "PS";
			else if(this.getObject() instanceof SuperPacGum)
				str = "SP";
			else if(this.getObject() instanceof PacGum)
				str = "PG";
			else 
				str = "  ";
		else
			str = "XX";
		
		
		
		return str;
		
	}

	public int getPosX() {
		return _pos_x;
	}
	
	public int getPosY() {
		return _pos_y;
	}

	public void setPosX(int px){
		_pos_x = px;
	}
	
	public void setPosY(int py){
		_pos_y = py;
	}
	
	public boolean isCrossable() {
		return _crossable;
	}
	
	public Image getImage(){
		return _image;
	}
	
	public Line getParent(){
		return _parent;
	}
	
	public void setParent(Line l){
		_parent = l;
	}
	
	public void setCrossable(boolean crossable){
		_crossable = crossable;
		updateTexture();
	}
	
	private void updateTexture(){
		if(_crossable)
			_image = Texture.TEXTURE_GROUND.getTexture();
		else
			_image = Texture.TEXTURE_WALL.getTexture();
	}

}
