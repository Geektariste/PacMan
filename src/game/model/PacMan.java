package game.model;

import game.model.abstract_model.Actor;
import game.model.abstract_model.Character;
import game.model.abstract_model.Static;
import game.view.GameRootPane;
import javafx.geometry.Rectangle2D;

import common.model.Texture;

/**
 * Defines a PacMan in the game. Extends Character.
 */
public class PacMan extends Character {

	public static int SIZE = 25;

	private int _life;

	public PacMan(int pos_x, 
				  int pos_y, 
				  int speed, 
				  int life) {
		
		super(pos_x, pos_y, SIZE, 0, speed, Texture.TEXTURE_PACMAN_LEFT);
		_life = life;

	}

	@Override
	public int be_eaten() {
		// TODO Auto-generated method stub
		_life--;
		GameRootPane.getInstance().countDown();
		return _points;
	}

	@Override
	public void eat(Actor a) {
		GameRootPane.getInstance().getLaby().addScore(a.be_eaten());
	}

	@Override
	public void move(Labyrinth laby) {
		super.move(laby);

		int gridX = _pos_x;
		int gridY = _pos_y;

		Case c = laby.getMap().getCase(gridX / Case.SIZE, gridY / Case.SIZE);
		Static s = c.getObject();
		if (s != null) {
			if (detectStaticColision(s)) {
				eat(s);
				if (!(s instanceof Fruit))
					c.setObject(null);
			}
		}
	}
		
	
	/**
	 * Returns true if there is a collision between PacMan and the Static s.
	 * 
	 * @param s Static we are looking for a collision
	 * @return True if there is a collision. False otherwise.
	 */
	private boolean detectStaticColision(Static s){
		
		Rectangle2D playerRect = new Rectangle2D(_pos_x, _pos_y, _size, _size);
		Rectangle2D staticRect = new Rectangle2D(s.getPosX(), s.getPosY(), s.getSize(), s.getSize());
		
		return playerRect.contains(staticRect);
	}
	
	public int getLife(){
		return _life;
	}
}
