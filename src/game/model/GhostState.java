package game.model;

/**
 * Enum defining the different states of a Ghost.
 */
public enum GhostState {
	
	RANDOM(0),
	CHASING(1),
	DEAD(2),
	WAITING(3);
	
	private final int _state;
	
	private GhostState(int state){
		_state = state;
	}
	
}
