package common.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import xml.XmlSerialization;
import xml.model.Map;
import game.model.Labyrinth;
import game.view.MapSelectionPane;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import leaderboard.view.LeaderboardMapSelectionPane;
import common.MainWindow;
import common.exception.InvalidFormatException;
import common.exception.InvalidStructureException;
import editor.view.EditorMapSelectionPane;

public class MainMenuPane extends MenuPane{
	
	public MainMenuPane(){
		
		super();
		
		this.getStyleClass().add("main-menu-pane");

		// #######################################################
		
		Label tMenu = new Label("Menu");
		tMenu.setId("menuTitle");
		
		// #######################################################
		
		Label tPlay = new Label("Play !");
		
		Label tEditor = new Label("Edit map");
		
		Label tLeaderboards = new Label("Leaderboards");
		
		Label tOptions = new Label("Options");
		
		Label tQuit = new Label("Quit");
		
		// #######################################################
		
		tPlay.setOnMouseClicked(new EventHandler() {
			@Override
			public void handle(Event arg0){
				//MainWindow.getInstance().switchPane(GameRootPane.getInstance());
				MainWindow.getInstance().switchPane(new MapSelectionPane());
			}	
		});
		
		tEditor.setOnMouseClicked(new EventHandler() {
			@Override
			public void handle(Event arg0){
				MainWindow.getInstance().switchPane(new EditorMapSelectionPane());
			}	
		});
		
		tLeaderboards.setOnMouseClicked(new EventHandler() {
			@Override
			public void handle(Event arg0){
				MainWindow.getInstance().switchPane(new LeaderboardMapSelectionPane());
			}	
		});
		
		tOptions.setOnMouseClicked(new EventHandler() {
			@Override
			public void handle(Event arg0){
				MainWindow.getInstance().switchPane(new MainOptionsPane());
			}	
		});
		
		tQuit.setOnMouseClicked(new EventHandler() {
			@Override
			public void handle(Event arg0){		
				System.exit(0);
			}	
		});
		
		_menu.getChildren().addAll(tPlay, tEditor, tLeaderboards, tOptions, tQuit);
		
		this.setTop(tMenu);
		this.setCenter(_menu);
	}

}
