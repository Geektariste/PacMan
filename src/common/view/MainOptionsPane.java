package common.view;

import java.io.File;
import java.io.FilenameFilter;

import common.MainWindow;
import common.model.DefaultString;
import common.model.ThemeLoader;
import game.model.Case;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class MainOptionsPane extends VBox {
	
	private ToggleGroup _themesToggleGroup;
	private VBox _themes;
	
	public MainOptionsPane(){
		
		_themesToggleGroup = new ToggleGroup();
		_themes = new VBox();
		
		_themesToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

			public void changed(ObservableValue<? extends Toggle> prop, Toggle old_toggle, Toggle new_toggle) {
				
				if (_themesToggleGroup.getSelectedToggle() != null) 
					((ThemeLoader) _themesToggleGroup.getSelectedToggle().getUserData()).load();

			}
		});
		
		Label tGoToMenu = new Label("Return to main menu");
		tGoToMenu.setFont(new Font(30));
		
		tGoToMenu.setOnMouseClicked(new EventHandler(){
			@Override
			public void handle(Event event) {
				MainWindow.getInstance().switchPane(new MainMenuPane());
			}
		});
		

		this.getChildren().addAll(_themes, tGoToMenu);
		
		populate();

	}
	
	public void populate(){
		
		File parentFolder = new File(DefaultString.IMAGES);
		String[] themeFolders = parentFolder.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});
		
		for(String themeFolder : themeFolders){
			
			ThemeLoader theme = ThemeLoader.loadTheme(themeFolder);
			RadioButton radio = new RadioButton(theme.getName());
		
			radio.setUserData(theme);
			radio.setToggleGroup(_themesToggleGroup);
			
			_themes.getChildren().add(radio);
			
		}

	}

}
