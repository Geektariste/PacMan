package common.view;


import javafx.scene.layout.StackPane;

public abstract class AppWithMenuPane extends StackPane{
	
	protected MenuPane _menu;
	protected boolean _opened_menu;
	
	public AppWithMenuPane(){
		_opened_menu = false;
	}
	
	public abstract void openCloseMenu();

	public boolean isOpened(){
		return _opened_menu;
	}
	
}
