package common.view;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public abstract class MenuPane extends BorderPane{
	
	protected VBox _menu;
	
	public MenuPane(){
		_menu = new VBox();
		_menu.getStyleClass().add("vbox");
	}
	
}
