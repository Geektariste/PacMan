package common.view;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 *	An editable text on double click using reflexion.
 *	This was part of an older project during Mathieu's last year internship. 
 */

public class EditableText extends HBox{
	
	private Object _file;
	private String _fieldName;
	
	private Text text;
	private TextField textField;
			
	public EditableText(String fieldName, Object file){
		
		populate(fieldName, file);
		
	}
	
	/**
	 * Creates the content of the Label, defines the events, and restrict the Label/TextField change to avoid empty areas on empty text (see below).
	 * 
	 * @param fieldName
	 * @param file
	 */
	public void populate(String fieldName, Object file){
		
		_file = file;
		_fieldName = fieldName;
		
		String initialValue = "Unable to read value";
		
		try {
			initialValue = (String) _file.getClass().getMethod("get"+_fieldName).invoke(_file);
		} catch (Exception e) { e.printStackTrace(); }
		
		text = new Text(initialValue);
		textField = new TextField();
		
		textField.setMinHeight(20);
		
		/*
		 * Avoids an empty area unclickable
		 */
		
		if(initialValue != null && initialValue.length()>0)
			this.getChildren().add(text);
		else
			this.getChildren().add(textField);
		
		/*
		 * Events
		 */
		
		/*
		 * On double-click
		 */
		text.setOnMouseClicked(new EventHandler<MouseEvent>() {  
			@Override  
			public void handle(MouseEvent event) {  
				if (event.getClickCount()==2) {  
					textField.setText(text.getText());

					EditableText.this.getChildren().add(textField);
					EditableText.this.getChildren().remove(text);

					textField.requestFocus(); 
					textField.selectAll();  

				}  
			}});

		/*
		 * On action (tab, enter, ...)
		 */
		textField.setOnAction(new EventHandler<ActionEvent>() {  
			@Override  
			public void handle(ActionEvent event) {  
				
				text.setText(textField.getText());  

				if(textField.getText().length() > 0 && EditableText.this.getChildren().contains(textField)){
					EditableText.this.getChildren().add(text);
					EditableText.this.getChildren().remove(textField);
				}

				try {
					_file.getClass().getMethod("set"+_fieldName, java.lang.String.class).invoke(_file, text.getText());
				} catch (Exception e) { e.printStackTrace(); }

			}

		});

		/*
		 * On focus change (to handle clicks in a void area to validate the field)
		 */
		textField.focusedProperty().addListener(new ChangeListener<Boolean>() {  
			@Override  
			public void changed(ObservableValue<? extends Boolean> observable,  
					Boolean oldValue, Boolean newValue) {  
				if (! newValue) {  
					text.setText(textField.getText());

					if(textField.getText().length() > 0 && EditableText.this.getChildren().contains(textField)){
						EditableText.this.getChildren().add(text);
						EditableText.this.getChildren().remove(textField);
					}
				}
				
				try {
					_file.getClass().getMethod("set"+_fieldName, java.lang.String.class).invoke(_file, text.getText());
				} catch (Exception e) { e.printStackTrace(); }
			}  
		}); 
		
	}
	
	/*
	 * Getters
	 */
	
	public Text getText(){ return text; }
	public TextField getTextField(){ return textField; }
}