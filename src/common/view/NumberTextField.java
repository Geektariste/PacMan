package common.view;

import javafx.scene.control.TextField;

public class NumberTextField extends TextField {
	
	public NumberTextField() {
		super();
	}

	@Override
	public void replaceText(int start, int end, String text) {
		if (validate(text)) {
			super.replaceText(start, end, text);
		}
	}

	@Override
	public void replaceSelection(String text) {
		if (validate(text)) {
			super.replaceSelection(text);
		}
	}

	private boolean validate(String text) {
		if (this.getText() != null) {
		}
		boolean status = ("".equals(text) || text.matches("[0-9]"));
		if (this.getText() == null) {
			return status;
		} else {
			return (status);
		}
	}
	
	/*
	 * Getter
	 */
	
	public int getValue(){
		return Integer.parseInt(this.textProperty().get());
	}
	
	/*
	 * Setter
	 */
	
	public void setValue(int value){
		this.setText(String.valueOf(value));
	}
}