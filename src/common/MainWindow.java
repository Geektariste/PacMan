package common;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import common.model.DefaultString;
import common.model.ThemeLoader;
import common.view.MainMenuPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Main to launch.
 */
public class MainWindow extends Application{
	
	/*
	 * Instance
	 */
	
	private static MainWindow INSTANCE = null;
	
	/*
	 * Main layouts
	 */
	
	private Scene scene;
	private Pane root;
	
	private MainMenuPane mainMenu;

	@Override
	public void start(Stage stage) throws Exception {
		
		INSTANCE = this;
		
		loadProperties();
		initUI(stage);
		
	}
	
	private void initUI(Stage stage) {
		
		root = new Pane();
		mainMenu = new MainMenuPane();
        
        scene = new Scene(root, 600, 600, Color.BLACK);
        //scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        scene.getStylesheets().add("./common/view/style.css");
        
        switchPane(mainMenu);
        
        stage.setTitle("PacMan");
        stage.setScene(scene);
        stage.setResizable(true);

        stage.show();
        
    }

	
	public static void main(String[] args) {
        launch(args);
    }
	
	public void switchPane(Pane pane){
		if(!root.getChildren().isEmpty())
			root.getChildren().remove(0);
		root.getChildren().add(pane);

        scene.setOnKeyPressed(pane.getOnKeyPressed());
	}
	
	public static MainWindow getInstance(){
		return INSTANCE;
	}
	
	public static void loadProperties(){
		
		Properties prop = new Properties();
		
		try {
			InputStream config = new FileInputStream(DefaultString.RESOURCES + DefaultString.CONFIG);
			prop.load(config);
			
			String current_theme = prop.getProperty("CURRENT_THEME");
			
			ThemeLoader.loadTheme(current_theme).load();;
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} 
	}
	
	public void saveProperties(){
		Properties prop = new Properties();
		
		try {
			InputStream config = new FileInputStream(DefaultString.RESOURCES + DefaultString.CONFIG);
			prop.load(config);
			
			prop.setProperty("CURRENT_THEME", ThemeLoader.getCurrent().getFolder());
			
			OutputStream save = new FileOutputStream(DefaultString.RESOURCES + DefaultString.CONFIG);
			prop.store(save, "");
			
		} catch (IOException e) {
			e.printStackTrace();
			
		} 
	}

}
