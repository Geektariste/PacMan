package common.model;

public class DefaultString {
	
	public static final String TEXTURE_PREFIX = "TEXTURE_";
	public static final String SIZE_PREFIX = "SIZE_";
	
	/*
	 * Resources
	 */
	public static final String RESOURCES = "resources/";
	public static final String IMAGES = RESOURCES+"img/";
	public static final String MAPS = RESOURCES+"maps/";
	public static final String SCOREBOARDS = RESOURCES+"scoreboards/";

	
	// File
	public static final String FILE = "file:";
	public static final String MAP_EXTENSION = ".pacmap";
	public static final String SCORE_EXTENSION = ".pacsc";
	public static final String DEFAULT_MAP = DefaultString.MAPS + "default/default" + DefaultString.MAP_EXTENSION;

	
	public static final String FILE_CHOOSER_ALL_MAPS = "*" + MAP_EXTENSION;
	public static final String FILE_CHOOSER_DESCRIPTION = "Pacmap files (" + FILE_CHOOSER_ALL_MAPS + ")";

	
	// Theme
	public static final String DEFAULT_THEME = "default";
	
	public static final String CONFIG = "config.properties";
	public static final String SIZES_CONFIG = "sizes.properties";
	public static final String TEXTURES_CONFIG = "textures.properties";
}
