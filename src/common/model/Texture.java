package common.model;

import game.model.Case;
import game.model.Fruit;
import game.model.Ghost;
import game.model.PacGum;
import game.model.PacMan;
import game.model.SuperPacGum;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public enum Texture {
	
	/*
	 * Enum
	 */
	
	// Background
	TEXTURE_GROUND,
	TEXTURE_WALL,
	
	// Statics
	TEXTURE_FRUIT,
	TEXTURE_PACGUM,
	TEXTURE_SUPERGUM,
	
	/*
	 * Dynamics
	 */
	
	// PacMan
	TEXTURE_PACMAN_DOWN,
	TEXTURE_PACMAN_LEFT,
	TEXTURE_PACMAN_UP,
	TEXTURE_PACMAN_RIGHT,
	
	// Ghosts
	TEXTURE_GHOST_1,
	TEXTURE_GHOST_2,
	TEXTURE_GHOST_3,
	TEXTURE_GHOST_4,
	
	TEXTURE_DEAD_LEFT,
	TEXTURE_VULNERABLE, 
	
	/*
	 * Editor
	 */
	TEXTURE_GHOST_SPAWN,
	TEXTURE_PACMAN_SPAWN,
	TEXTURE_SUPERGUM_SPAWN,
	TEXTURE_FRUIT_SPAWN;
	
	
	private static Texture CURRENT_TEXTURE = TEXTURE_WALL;
	
	private Image _texture;
	private int _width;

	/*
	 * Getters
	 */
	
	public Image getTexture(){
		return _texture;
	}
	
	public int getWidth(){
		return _width;
	}
	
	/*
	 * Setters
	 */
	
	public void setTexture(Image texture){
		_texture = texture;
	}
	
	public void setWidth(int width){
		_width = width;
	}
	
	/*
	 * Static functions
	 */
	public static Group createSpawnImage(){
		return new Group(
						new ImageView(TEXTURE_GROUND.getTexture()), 
						new ImageView(Texture.getCurrentTexture().getTexture())
					);
	}
	
	public static Group createSpawnImage(Texture texture){
		return new Group(
						new ImageView(TEXTURE_GROUND.getTexture()), 
						new ImageView(texture.getTexture())
					);
	}
	
	public static Texture getCurrentTexture(){
		return CURRENT_TEXTURE;
	}
	
	public static void setCurrentTexture(Texture texture){
		CURRENT_TEXTURE = texture;
	}
	
	public static void reloadWidths(){
		TEXTURE_GROUND.setWidth(Case.SIZE);
		TEXTURE_WALL.setWidth(Case.SIZE);
		
		TEXTURE_FRUIT.setWidth(Fruit.SIZE);
		TEXTURE_PACGUM.setWidth(PacGum.SIZE);
		TEXTURE_SUPERGUM.setWidth(SuperPacGum.SIZE);
		
		TEXTURE_PACMAN_DOWN.setWidth(PacMan.SIZE);
		TEXTURE_PACMAN_LEFT.setWidth(PacMan.SIZE);
		TEXTURE_PACMAN_UP.setWidth(PacMan.SIZE);
		TEXTURE_PACMAN_RIGHT.setWidth(PacMan.SIZE);
		
		TEXTURE_GHOST_1.setWidth(Ghost.SIZE);
		TEXTURE_GHOST_2.setWidth(Ghost.SIZE);
		TEXTURE_GHOST_3.setWidth(Ghost.SIZE);
		TEXTURE_GHOST_4.setWidth(Ghost.SIZE);
		
		TEXTURE_DEAD_LEFT.setWidth(Ghost.SIZE);
		TEXTURE_VULNERABLE.setWidth(Ghost.SIZE); 
		

		TEXTURE_GHOST_SPAWN.setWidth(Case.SIZE);
		TEXTURE_PACMAN_SPAWN.setWidth(Case.SIZE);
		TEXTURE_SUPERGUM_SPAWN.setWidth(Case.SIZE);
		TEXTURE_FRUIT_SPAWN.setWidth(Case.SIZE);
	}

}
