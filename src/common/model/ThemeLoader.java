package common.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import common.MainWindow;
import javafx.scene.image.Image;

public class ThemeLoader {
	
	public static ThemeLoader CURRENT_THEME;
	
	private String _themeFolder;
	private String _name;
	
	public ThemeLoader(String themeFolder, String name){
		
		_themeFolder = themeFolder;
		_name = name;
		
	}
	
	public void load(){
		
		loadSizes(DefaultString.IMAGES + _themeFolder);
		Texture.reloadWidths();
		loadTextures(DefaultString.IMAGES + _themeFolder);
		CURRENT_THEME = this;
		MainWindow.getInstance().saveProperties();
	}
	
	public String getName(){
		return _name;
	}
	
	public String getFolder(){
		return _themeFolder;
	}
	
	/*
	 * 
	 */
	
	public static ThemeLoader loadTheme(String themeFolderName){
		
		String themeFolder = themeFolderName + "/";
		Properties prop = new Properties();

		try {
			InputStream config = new FileInputStream(DefaultString.IMAGES + themeFolder + DefaultString.CONFIG);
			prop.load(config);
			
			String name = prop.getProperty("name");
			
			return new ThemeLoader(themeFolder, name);
			
		} catch (IOException e) {
			e.printStackTrace();
			
			return null;
		} 
		
		
	}
	
	public static void loadSizes(String themeFolder){
		
		Properties prop = new Properties();

		try {
			InputStream config = new FileInputStream(themeFolder + DefaultString.SIZES_CONFIG);
			prop.load(config);
			
			for(Object property : prop.keySet()){
				
				if (((String) property).startsWith(DefaultString.SIZE_PREFIX)) {
					
					try {
						Class<?> to_change = Class.forName(
								"game.model." + ((String) property).substring(DefaultString.SIZE_PREFIX.length()));
						
						to_change.getField("SIZE").set(Integer.class, Integer.parseInt((String) prop.get((String) property)));
						
					} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
							| SecurityException | ClassNotFoundException e) {
						e.printStackTrace();
					}
				}				
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public static void loadTextures(String themeFolder){
		
		Properties prop = new Properties();

		try {
			InputStream config = new FileInputStream(themeFolder + DefaultString.TEXTURES_CONFIG);
			prop.load(config);
			
			for(Object property : prop.keySet()){
				
				if (((String) property).startsWith(DefaultString.TEXTURE_PREFIX)) {

					Texture to_load = Enum.valueOf(Texture.class, (String) property);
					to_load.setTexture(new Image(

							DefaultString.FILE + themeFolder + prop.getProperty((String) property), to_load.getWidth(),
							to_load.getWidth(), true, false));

				}				
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public static ThemeLoader getCurrent(){
		return CURRENT_THEME;
	}

}
