package common.exception;

public class InvalidFormatException extends Exception{

	public String toString(){
		return "Error : Invalid map format\n";
	}
	
}
