package common.exception;

/**
 * Raised when the Map has an invalid structure.
 */
public class InvalidStructureException extends Exception{

	public String toString(){
		return "Error : Invalid maps structure\n";
	}
	
}
